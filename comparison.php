<?php include 'inc/header.php'; ?>
	<!-- page head -->
	<div class="wrapper img-header"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 breadcrumbs">
				<ul>
					<li><a href="#">Главная</a></li><span>&rsaquo;</span>
					<li><a href="#">Корзина</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- tile -->
	<div class="container">
		<header>
			<h1 class="page-title">Сравнение товаров</h1>
		</header>
	</div>

	<!-- catalog -->
	<div class="container comparison-page">
		<div class="row">
			<div class="col-xs-3 comparison-left">
				<table>
					<tr>
						<td>
							<div class="item">
								<a href="#" class="btn btn-default page-next">Вернуться к Колонкам</a>
							</div>
						</td>
					</tr>
					<tr>
						<td><strong>Номинальная мощность</strong></td>
					</tr>
					<tr>
						<td><strong>Номинальная мощность</strong></td>
					</tr>
					<tr>
						<td><strong>Номинальная мощность</strong></td>
					</tr>
					<tr>
						<td><strong>Номинальная мощность</strong></td>
					</tr>
					<tr>
						<td><strong>Номинальная мощность</strong></td>
					</tr>
				</table>
			</div>
			<div class="col-xs-9 comparison-right">
				<div class="double-scroll">
					<table>
						<!-- item -->
						<tr>
							<td>
								<div class="item">
									<a href="#" class="item__addto delete"><span>Удалить из сравнения</span></a>
									<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
										<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
										<div class="item__new">Новинка</div>
										<!-- <div class="item__sale">Скидка</div> -->
										<!-- <div class="item__hit">Хит</div> -->
									</a> <!-- link item -->
									<p class="item__date">Неделю назад</p>
									<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
									<p class="item__cost">3 268 000 Br</p>
									<p class="item__description">Микрофон на гусиной шее</p>
									<p class="item__characters">250 Ом / шея 40,5 см</p>
								</div>
							</td>
							<td>
								<div class="item">
									<a href="#" class="item__addto delete"><span>Удалить из сравнения</span></a>
									<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
										<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
										<div class="item__new">Новинка</div>
										<!-- <div class="item__sale">Скидка</div> -->
										<!-- <div class="item__hit">Хит</div> -->
									</a> <!-- link item -->
									<p class="item__date">Неделю назад</p>
									<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
									<p class="item__cost">3 268 000 Br</p>
									<p class="item__description">Микрофон на гусиной шее</p>
									<p class="item__characters">250 Ом / шея 40,5 см</p>
								</div>
							</td>
							<td>
								<div class="item">
									<a href="#" class="item__addto delete"><span>Удалить из сравнения</span></a>
									<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
										<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
										<div class="item__new">Новинка</div>
										<!-- <div class="item__sale">Скидка</div> -->
										<!-- <div class="item__hit">Хит</div> -->
									</a> <!-- link item -->
									<p class="item__date">Неделю назад</p>
									<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
									<p class="item__cost">3 268 000 Br</p>
									<p class="item__description">Микрофон на гусиной шее</p>
									<p class="item__characters">250 Ом / шея 40,5 см</p>
								</div>
							</td>
							<td>
								<div class="item">
									<a href="#" class="item__addto delete"><span>Удалить из сравнения</span></a>
									<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
										<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
										<div class="item__new">Новинка</div>
										<!-- <div class="item__sale">Скидка</div> -->
										<!-- <div class="item__hit">Хит</div> -->
									</a> <!-- link item -->
									<p class="item__date">Неделю назад</p>
									<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
									<p class="item__cost">3 268 000 Br</p>
									<p class="item__description">Микрофон на гусиной шее</p>
									<p class="item__characters">250 Ом / шея 40,5 см</p>
								</div>
							</td>
							<td>
								<div class="item">
									<a href="#" class="item__addto delete"><span>Удалить из сравнения</span></a>
									<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
										<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
										<div class="item__new">Новинка</div>
										<!-- <div class="item__sale">Скидка</div> -->
										<!-- <div class="item__hit">Хит</div> -->
									</a> <!-- link item -->
									<p class="item__date">Неделю назад</p>
									<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
									<p class="item__cost">3 268 000 Br</p>
									<p class="item__description">Микрофон на гусиной шее</p>
									<p class="item__characters">250 Ом / шея 40,5 см</p>
								</div>
							</td>
						</tr>
						<tr>
							<td>400 Вт</td>
							<td>400 Вт</td>
							<td>400 Вт</td>
							<td>400 Вт</td>
							<td>400 Вт</td>
						</tr>
						<tr>
							<td>800 Вт</td>
							<td>800 Вт</td>
							<td>800 Вт</td>
							<td>800 Вт</td>
							<td>800 Вт</td>
						</tr>
						<tr>
							<td>-3 дБ  50 – 20 000 Гц</td>
							<td>-3 дБ  50 – 20 000 Гц</td>
							<td>-3 дБ  50 – 20 000 Гц</td>
							<td>-3 дБ  50 – 20 000 Гц</td>
							<td>-3 дБ  50 – 20 000 Гц</td>
						</tr>
						<tr>
							<td>127 дБ</td>
							<td>127 дБ</td>
							<td>127 дБ</td>
							<td>127 дБ</td>
							<td>127 дБ</td>
						</tr>
						<tr>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<!-- end item -->
					</table>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="clearfix"></div>
				<a href="#" class="btn btn-default page-next alignleft">Вернуться к Колонкам</a>
			</div>
		</div>
	</div>

<?php include 'inc/footer.php'; ?>