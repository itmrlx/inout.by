<?php include 'inc/header.php'; ?>
	<!-- page head -->
	<div class="wrapper img-header"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 breadcrumbs">
				<ul>
					<li><a href="#">Главная</a></li><span>&rsaquo;</span>
					<li><a href="#">О компании</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- title -->
	<div class="container">
		<header>
			<h1 class="title-journal">О компании</h1>
		</header>
	</div>

	<!-- advantages -->
	<div class="container about-advantages">
		<div class="row">
			<div class="sp2">
				<img src="img/advantages1.png" alt="advantages">
				<div class="title">Индивидуальное<br>проектирование</div>
			</div>
			<div class="sp2">
				<img src="img/advantages2.png" alt="advantages">
				<div class="title">Быстрая<br>поставка</div>
			</div>
			<div class="sp2">
				<img src="img/advantages3.png" alt="advantages">
				<div class="title">Качесвенная<br>установка</div>
			</div>
			<div class="sp2">
				<img src="img/advantages4.png" alt="advantages">
				<div class="title">Квалифицированная<br>настройка</div>
			</div>
			<div class="sp2">
				<img src="img/advantages5.png" alt="advantages">
				<div class="title">Обслуживание<br>оборудования</div>
			</div>
		</div>
	</div>

	<!-- about text -->
	<div class="container about-text">
		<div class="row">
			<div class="col-xs-6">
				<h2>Как заказать надежную, качественную и удобную в использовании трансляционную систему или профессиональное звуковое оборудование.</h2>
				<p>Если перед вами стоит такая задача - обращайтесь в ИНАУТ. Мы реализовали за 7 лет работы десятки различных проектов. И сегодня опыт и квалификация наших инженеров позволяет нам решать самые сложные и нестандартные задачи, которые ставит заказчик. При этом ему не придется переплачивать за лишние функции в оборудовании. Клиент получит аудиовизуальную систему именно с теми техническими возможностями, которые ему необходимы.</p>
				<p>Наша компания зарабатывает за счет профессиональных услуг по проектированию, инсталляции и обслуживанию AV-систем. Поэтому у нас нет необходимости по максимуму начинять зал или другие объекты оборудованием которое используется не по назначению. Мы пытаемся продать клиенту ничего лишнего. Разработанная в ИНАУТ спецификация будет отвечать запросам клиента и особенностям конкретного помещения, его размерам, акустике и освещенности.</p>
				<p><strong>Фишка нашей компании </strong></p>
				<p>Наша фишка состоит в том, что за 7 лет работы мы сумели подстроиться под бюджетный рынок белорусских заказов. Очень часто клиенты просят, чтобы в рамках ограниченных ресурсов получилась качественная и надежная система. И мы такую систему делаем.</p>
				<p>Это удается потому, что наши специалисты постоянно повышают свою квалификацию. Мы регулярно посещаем семинары крупных мировых производителей, где внимательно изучаем особенности и возможности нового оборудования, включая совместимость по техническим протоколам. Наши инженеры знают – на какой бренд обратить внимание, чтобы обеспечить необходимые для заказчика функции оборудования и качество. Мы составляем из продукции разных марок надежные и удобные в управлении AV- системы.</p>
				<p>Такой подход позволяет сэкономить для клиента огромные деньги. <strong>Благодаря нашим техническим решениям он платит в два, а порой и в три раза меньше, чем закупить оборудование просто у дорого бренда.</strong></p>
			</div>
			<div class="col-xs-6">
				<p>Если клиенту нужна статусная профессиональная система от одной, дорогой торговой марки – мы с удовольствием берем такие заказы. Тогда всё оборудование: от сложной аппаратуры до кабелей и мелких аксессуаров – будет из одних рук, с одинаковым уровнем качества и идеальной совместимостью. Вы также получите единую гарантию по всей системе.</p>
				<p><strong>Наше оборудование: ИНАУТ</strong></p>
				<p>работает только с профессиональной продукцией. За 7 лет работы наши специалисты хорошо изучили международный рынок производителей оборудования. Благодаря этому мы обеспечиваем комплексные прямые поставки. Всё необходимое для ваших аудиовизуальных систем, включая кабели и расходные материалы, а также эксклюзивное и нестандартное оборудование - вы сможете приобрести у нас. Широкий портфель предложений наших постоянных поставщиков позволяет комплектовать как бюджетные AV-системы, так и заказы премиум.</p>
				<p><strong>Официальное представительство</strong></p>
				<p>ИНАУТ является официальным представителем в Беларуси испанской фирмы «FoneStar» <a href="#">(www.fonestar.by)</a>. Она выпускает трансляционное оборудование высокого качества в среднем ценовом сегменте. Настенными и потолочными громкоговорителями, микрофонами, усилители мощности и другой продукцией «FoneStar» озвучено множество объектов по всему миру. (ссылка). Так же является официальным представителем Французкой фирмы «Universal-Effects» <a href="#">(www.universal-effects.by)</a>. Фирма делает очень качественные и сертифицированные жидкости для сценических спецэффектов. Дым жидкости, туман жидкости, жидкости для мыльных пузырей, снег жидкости, жидкости для генераторов пены и мн.др. Все это есть у нас на складе в Минске.</p>
				<p>Как мы оказываем услуги: по системам мультимедиа мы можем сделать всё. ИНАУТ гибко подходит к запросам клиентам, не навязывая им полный комплекс услуг. Наши специалисты готовы подключиться к решению задач заказчика на любом этапе разработки, инсталляции и обслуживания AV-системы. Мы также обеспечим её работу всем необходимым - как по «своим» проектам и оборудованию, так и по «чужим». Подробнее – здесь:</p>
				<p>Наши <a href="#">гарантии и порядок оказания услуг</a></p>
				<p>Обращайтесь в ИНАУТ с любыми вопросами по слаботочным системам – всегда вам поможем.</p>
			</div>
		</div>
	</div>

	<!-- our command -->
	<div class="wrapper our-command-wr">
		<div class="container">
			<div class="slick-about">
				<div class="slide">
					<div class="img-block">
						<img src="img/content/about-people2.png" alt="about">
					</div>
					<div class="name">Екатерина Киреева</div>
					<div class="status">Бухгалтер</div>
					<div class="desc">Переговоры и прием заказов по проектированию аудиовизуальных и световых систем, заключение договоров, бесплатный выезд на объект. Сотрудничество с дилерами.<br>
					Velcome: <strong>+375 29 611-71-10 </strong>
					</div>
				</div>
				<div class="slide">
					<div class="img-block">
						<img src="img/content/about-people3.png" alt="about">
					</div>
					<div class="name">Денис Чутьянов</div>
					<div class="status">Инженер</div>
					<div class="desc">Переговоры и прием заказов по проектированию аудиовизуальных и световых систем, заключение договоров, бесплатный выезд на объект. Сотрудничество с дилерами.<br>
					Velcome: <strong>+375 29 611-71-10 </strong>
					</div>
				</div>
				<div class="slide">
					<div class="img-block">
						<img src="img/content/about-people1.png" alt="about">
					</div>
					<div class="name">Руслан Якунин</div>
					<div class="status">Директор</div>
					<div class="desc">Переговоры и прием заказов по проектированию аудиовизуальных и световых систем, заключение договоров, бесплатный выезд на объект. Сотрудничество с дилерами.<br>
					Velcome: <strong>+375 29 611-71-10 </strong>
					</div>
				</div>
				<div class="slide">
					<div class="img-block">
						<img src="img/content/about-people4.png" alt="about">
					</div>
					<div class="name">Валерия Радченко</div>
					<div class="status">Секретарь</div>
					<div class="desc">Переговоры и прием заказов по проектированию аудиовизуальных и световых систем, заключение договоров, бесплатный выезд на объект. Сотрудничество с дилерами.<br>
					Velcome: <strong>+375 29 611-71-10 </strong>
					</div>
				</div>
				<div class="slide">
					<div class="img-block">
						<img src="img/content/about-people5.png" alt="about">
					</div>
					<div class="name">Илья Мажута</div>
					<div class="status">Контент менеджер</div>
					<div class="desc">Переговоры и прием заказов по проектированию аудиовизуальных и световых систем, заключение договоров, бесплатный выезд на объект. Сотрудничество с дилерами.<br>
					Velcome: <strong>+375 29 611-71-10 </strong>
					</div>
				</div>
				<div class="slide">
					<div class="img-block">
						<img src="img/content/about-people1.png" alt="about">
					</div>
					<div class="name">Руслан Якунин</div>
					<div class="status">Директор</div>
					<div class="desc">Переговоры и прием заказов по проектированию аудиовизуальных и световых систем, заключение договоров, бесплатный выезд на объект. Сотрудничество с дилерами.<br>
					Velcome: <strong>+375 29 611-71-10 </strong>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- other command -->
	<div class="container other-command">
		<div class="title">Иногда мы привлекаем узких специалистов</div>
		<div class="col-xs-6 other-command-item">
			<div class="row">
				<div class="col-xs-3">
					<img src="img/content/spec1.png" alt="spec">
				</div>
				<div class="col-xs-9">
					<div class="name">Иван Иванович Иванов</div>
					<div class="spec">Инженер-технолог</div>
					<div class="spec2">БНТУ > прикладная инженерия</div>
					<ul>
						<li>Монтаж оборудования</li>
						<li>Натройка аппаратуры</li>
						<li>Ведение селького хозяйства</li>
						<li>Уникальные навыки в области электроники</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-xs-6 other-command-item">
			<div class="row">
				<div class="col-xs-3">
					<img src="img/content/spec2.png" alt="spec">
				</div>
				<div class="col-xs-9">
					<div class="name">Петр Петрович Петров</div>
					<div class="spec">Инженер-технолог</div>
					<div class="spec2">БГУ > монтажное искусство</div>
					<ul>
						<li>Монтаж оборудования</li>
						<li>Натройка аппаратуры</li>
						<li>Ведение селького хозяйства</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-xs-6 other-command-item">
			<div class="row">
				<div class="col-xs-3">
					<img src="img/content/spec3.png" alt="spec">
				</div>
				<div class="col-xs-9">
					<div class="name">Валерий Иванович Сидоров</div>
					<div class="spec">Инженер-технолог</div>
					<div class="spec2">Нархоз > установка оборудования</div>
					<ul>
						<li>Навыки в области электроники</li>
						<li>Монтаж оборудования</li>
						<li>Натройка аппаратуры</li>
						<li>Ведение селького хозяйства</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-xs-6 other-command-item">
			<div class="row">
				<div class="col-xs-3">
					<img src="img/content/spec4.png" alt="spec">
				</div>
				<div class="col-xs-9">
					<div class="name">Анна Иванович Иванов</div>
					<div class="spec">Инженер-технолог</div>
					<div class="spec2">БНТУ > прикладная инженерия</div>
					<ul>
						<li>Уникальные навыки в области электроники</li>
						<li>Натройка аппаратуры</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!-- about clients -->
	<div class="container about-clients">
		<div class="title">Наши основные клиенты</div>
		<div class="row">
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
			<div class="col-xs-3">
				<a href="#">
					<img src="img/content/about-client.png" alt="img">
					<span class="name">Складские помещения и производства</span>
				</a>
			</div>
		</div>
	</div>

	<!-- about add description -->
	<div class="container about-add-description">
		<div class="row">
			<div class="col-xs-6">
				<p><strong>Компания ИНАУТ обеспечивает полный комплекс услуг в области проектирования и инсталляции профессиональных мультимедиа-систем.</strong></p>
				<ul>
					<li>Оборудование для конференц-связи</li>
					<li>Оборудование для видео конференц-связи</li>
					<li>Системы звуко-усиления и речевого оповещения</li>
					<li>Интерактивные кафедры</li>
					<li>Оборудование для синхронного перевода</li>
					<li>Беспроводная система голосования</li>
					<li>Проекционное оборудование, системы видео отображения</li>
					<li>Системы управления</li>
				</ul>
			</div>
			<div class="col-xs-6">
				<p><strong>Принцип работы. Алгоритм.</strong></p>
				<p>Вы ставите нам задачу, а мы ее максимально быстро, качественно и в срок реализуем! Каждый проект мы тщательно продумываем и доводим до реализации, контролируя все этапы работ.</p>
			</div>
		</div>
	</div>

	<!-- about contacts -->
	<div class="container">
		<div class="about-contacts">
			<div class="title"><span>Частное предприятие<br>"ИНАУТ"</span></div>
			<div class="row">
				<div class="col-xs-6 phones">
					<p><span>Тел.\факс:</span> +375-17-285-15-83.</p>
					<p><span>Velcom:</span> +375-29-611-71-10.</p>
					<p><span>Life:</span> +375-25-611-71-10.</p>
				</div>
				<div class="col-xs-6 contacts">
					Мы работотаем с 9 до 17:00<br>
					кроме субботы и воскресенья<br>
					г. Минск, ул. Чижевских, 172.
				</div>
			</div>
		</div>
	</div>


<?php include 'inc/footer.php'; ?>