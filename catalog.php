<?php include 'inc/header.php'; ?>
	<!-- page head -->
	<div class="wrapper img-header"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 breadcrumbs">
				<ul>
					<li><a href="#">Главная</a></li><span>&rsaquo;</span>
					<li><a href="#">Корзина</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- tile -->
	<div class="container">
		<header>
			<h1 class="page-title">Колонки</h1>
		</header>
	</div>

	<!-- catalog -->
	<div class="container catalog-container">
		<div class="row">
			<!-- catalog sidebar -->
			<div class="col-xs-3 catalog-sidebar">
				<!-- sidebar block -->
				<div class="catalog-sidebar-block">
					<h2 class="catalog-sidebar-title">Производитель</h2>
					<div class="checkbox-style">
						<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_0" value="Apart">
						<label for="mse2_msvendor|name_0" class="">
							 Alto 
						</label>
					</div>
					<div class="checkbox-style">
						<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_1" value="Apart">
						<label for="mse2_msvendor|name_1" class="">
							 Begringer 
						</label>
					</div>
					<div class="checkbox-style">
						<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_2" value="Apart">
						<label for="mse2_msvendor|name_2" class="">
							 Biema 
						</label>
					</div>
					<div class="checkbox-style">
						<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_3" value="Apart">
						<label for="mse2_msvendor|name_3" class="">
							 Bose 
						</label>
					</div>
					<div class="checkbox-style">
						<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_4" value="Apart">
						<label for="mse2_msvendor|name_4" class="">
							 Boway 
						</label>
					</div>
					<div class="all-variants"><span>Все 24 варианта</span></div>
					<!-- modal -->
					<div class="all-variants-modal">
						<div class="checkbox-style">
							<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_5" value="Apart">
							<label for="mse2_msvendor|name_5" class="">
								 Alto 
							</label>
						</div>
						<div class="checkbox-style">
							<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_6" value="Apart">
							<label for="mse2_msvendor|name_6" class="">
								 Begringer 
							</label>
						</div>
						<div class="checkbox-style">
							<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_7" value="Apart">
							<label for="mse2_msvendor|name_7" class="">
								 Biema 
							</label>
						</div>
						<div class="checkbox-style">
							<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_8" value="Apart">
							<label for="mse2_msvendor|name_8" class="">
								 Bose 
							</label>
						</div>
						<div class="checkbox-style">
							<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_9" value="Apart">
							<label for="mse2_msvendor|name_9" class="">
								 Boway 
							</label>
						</div>
					</div>
				</div>
				<!-- end sidebar block -->
				<!-- sidebar block -->
				<div class="catalog-sidebar-block">
					<h2 class="catalog-sidebar-title">Тип</h2>
					<div class="checkbox-style">
						<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_10" value="Apart">
						<label for="mse2_msvendor|name_10" class="">
							 Комплект 
						</label>
					</div>
					<div class="checkbox-style">
						<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_11" value="Apart">
						<label for="mse2_msvendor|name_11" class="">
							 Линейный массив 
						</label>
					</div>
					<div class="checkbox-style">
						<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_12" value="Apart">
						<label for="mse2_msvendor|name_12" class="">
							 Монитор 
						</label>
					</div>
					<div class="all-variants"><span>Все 6 вариантов</span></div>
					<!-- modal -->
					<div class="all-variants-modal">
						<div class="checkbox-style">
							<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_13" value="Apart">
							<label for="mse2_msvendor|name_13" class="">
								 Комплект 
							</label>
						</div>
						<div class="checkbox-style">
							<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_14" value="Apart">
							<label for="mse2_msvendor|name_14" class="">
								 Линейный массив 
							</label>
						</div>
						<div class="checkbox-style">
							<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_15" value="Apart">
							<label for="mse2_msvendor|name_15" class="">
								 Монитор 
							</label>
						</div>
					</div>
				</div>
				<!-- end sidebar block -->
				<!-- sidebar block -->
				<div class="catalog-sidebar-block">
					<h2 class="catalog-sidebar-title">Мощность</h2>
					<div class="checkbox-style">
						<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_16" value="Apart">
						<label for="mse2_msvendor|name_16" class="">
							 100 Вт 
						</label>
					</div>
					<div class="checkbox-style">
						<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_17" value="Apart">
						<label for="mse2_msvendor|name_17" class="">
							 1000 Вт 
						</label>
					</div>
					<div class="checkbox-style">
						<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_18" value="Apart">
						<label for="mse2_msvendor|name_18" class="">
							 1100 Вт 
						</label>
					</div>
					<div class="all-variants"><span>Все 9 вариантов</span></div>
					<!-- modal -->
					<div class="all-variants-modal">
						<div class="checkbox-style">
							<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_19" value="Apart">
							<label for="mse2_msvendor|name_19" class="">
								 add 
							</label>
						</div>
						<div class="checkbox-style">
							<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_20" value="Apart">
							<label for="mse2_msvendor|name_20" class="">
								 add 
							</label>
						</div>
						<div class="checkbox-style">
							<input type="checkbox" name="msvendor|name" id="mse2_msvendor|name_21" value="Apart">
							<label for="mse2_msvendor|name_21" class="">
								 add 
							</label>
						</div>
					</div>
				</div>
				<!-- end sidebar block -->
			</div>
			<!-- end catalog sidebar -->
			<!-- catalog content -->
			<div class="col-xs-9">
				<div class="row catalog-content-header">
					<div class="col-xs-9">
						<div class="check-variable">Alto</div>
						<div class="check-variable">Behringer</div>
						<div class="check-variable">Biema</div>
						<div class="check-variable">Bose</div>
						<div class="clearfix"></div>
						<div class="sort-pimary">
							<a href="#">Портативные колонки</a>
							<a href="#">Колонки для больших залов</a>
							<a href="#">Клубные колонки</a>
							<a href="#">Портативные колонки</a>
							<a href="#">Колонки для больших залов</a>
							<a href="#">Клубные колонки</a>
						</div>
					</div>
					<div class="col-xs-3">
						<a href="#" class="btn btn-default alignright btn-compare">Сравнить 3 товара</a>
					</div>
					<div class="col-xs-12 sort">
						<span>Сортировать по: </span>
						<a href="#" class="sort-select active">Популярности</a> <!-- add class 'back' -->
						<a href="#" class="sort-select">Цене</a>
						<a href="#" class="sort-select">Дате</a>
					</div>
				</div>
				<div class="row catalog-items">
					<!-- items -->
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="comparison-add comparison-link item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<div class="item__new">Новинка</div>
								<!-- <div class="item__sale">Скидка</div> -->
								<!-- <div class="item__hit">Хит</div> -->
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее Микрофон на гусиной шее Микрофон на гусиной шее  </p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<!-- <div class="item__new">Новинка</div> -->
								<div class="item__sale">Скидка</div>
								<!-- <div class="item__hit">Хит</div> -->
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее</p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<!-- <div class="item__new">Новинка</div> -->
								<!-- <div class="item__sale">Скидка</div> -->
								<!-- <div class="item__hit">Хит</div> -->
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее</p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<!-- <div class="item__new">Новинка</div> -->
								<!-- <div class="item__sale">Скидка</div> -->
								<div class="item__hit">Хит</div>
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее</p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<div class="item__new">Новинка</div>
								<!-- <div class="item__sale">Скидка</div> -->
								<!-- <div class="item__hit">Хит</div> -->
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее</p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<!-- <div class="item__new">Новинка</div> -->
								<div class="item__sale">Скидка</div>
								<!-- <div class="item__hit">Хит</div> -->
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее</p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<!-- <div class="item__new">Новинка</div> -->
								<!-- <div class="item__sale">Скидка</div> -->
								<!-- <div class="item__hit">Хит</div> -->
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее</p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<!-- <div class="item__new">Новинка</div> -->
								<!-- <div class="item__sale">Скидка</div> -->
								<div class="item__hit">Хит</div>
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее</p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<div class="item__new">Новинка</div>
								<!-- <div class="item__sale">Скидка</div> -->
								<!-- <div class="item__hit">Хит</div> -->
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее</p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<!-- <div class="item__new">Новинка</div> -->
								<div class="item__sale">Скидка</div>
								<!-- <div class="item__hit">Хит</div> -->
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее</p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<!-- <div class="item__new">Новинка</div> -->
								<!-- <div class="item__sale">Скидка</div> -->
								<!-- <div class="item__hit">Хит</div> -->
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее</p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="item">
							<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
							<a href="catalog/pa-systems/microphone-goosneck/1112" class="item__image">
								<img src="http://images.inout.by/products/1112/150x150/fcm-764.jpg" alt="Микрофон на гусиной шее Fonestar FCM-764">
								<!-- <div class="item__new">Новинка</div> -->
								<!-- <div class="item__sale">Скидка</div> -->
								<div class="item__hit">Хит</div>
							</a> <!-- link item -->
							<p class="item__date">Неделю назад</p>
							<h2 class="item__title"><a href="catalog/pa-systems/microphone-goosneck/1112">Fonestar FCM-764</a></h2> <!-- link item -->
							<p class="item__cost">3 268 000 Br</p>
							<p class="item__description">Микрофон на гусиной шее</p>
							<p class="item__characters">250 Ом / шея 40,5 см</p>
						</div>
					</div>
					<!-- end items -->
					<div class="clearfix"></div>
					<div class="pager">
						<div class="col-xs-6 col-xs-offset-3">
							<a href="#" class="btn btn-default page-next">Показать следющие 20 товаров</a>
						</div>
						<div class="col-xs-3">
							<a href="#" class="btn btn-default alignright btn-compare">Сравнить 3 товара</a>
						</div>
						<div class="col-xs-12">
							<p>Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты. Города над не, но переписали грамматики переулка предупредила пустился путь знаках lorem имеет себя жаренные там, ее рекламных, собрал рукописи решила продолжил за грустный курсивных инициал журчит. Осталось повстречался переулка, запятых, последний своих подпоясал. Родного за скатился, то всеми свое?</p>
							<p>Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты. Города над не, но переписали грамматики переулка предупредила пустился путь знаках lorem имеет себя жаренные там, ее рекламных, собрал рукописи решила продолжил за грустный курсивных инициал журчит. Осталось повстречался переулка, запятых, последний своих подпоясал. Родного за скатился, то всеми свое?</p>
							<p>Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты. Города над не, но переписали грамматики переулка предупредила пустился путь знаках lorem имеет себя жаренные там, ее рекламных, собрал рукописи решила продолжил за грустный курсивных инициал журчит. Осталось повстречался переулка, запятых, последний своих подпоясал. Родного за скатился, то всеми свое?</p>
						</div>
					</div>
				</div>
			</div>
			<!-- end catalog content -->
		</div>
	</div>

<?php include 'inc/footer.php'; ?>