	<footer class="wrapper wr-blue footer">
		<div class="container footer-slider-wrapper">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="footer-slider-title">Наши клиенты</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 footer-slider-container">
					<div class="footer-slider">
						<a class="slide" href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
						<a class="slide" href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
						<a class="slide" href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
						<a class="slide" href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
						<a class="slide" href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
						<a class="slide" href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
						<a class="slide" href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
						<a class="slide" href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="centered">
						<div class="btn btn-default">Письма благодарности</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wrapper foo-menu">
			<div class="container">
				<div class="row">
					<div class="sp2">
						<header class="foo-menu__header">
							Системы оповещения и трансляции
						</header>
						<ul class="foo-menu__menu">
							<li><a href="#">Громкоговорители</a></li>
							<li><a href="#">Усилители 70/100V</a></li>
							<li><a href="#">Микрофоны настольные</a></li>
							<li><a href="#">Радиомикрофоны</a></li>
							<li><a href="#">Конференц системы</a></li>
							<li><a href="#">Мегафоны</a></li>
							<li><a href="#">Проигрыватели</a></li>
						</ul>
					</div>
					<div class="sp2">
						<header class="foo-menu__header">
							Концертное звукоусиление
						</header>
						<ul class="foo-menu__menu">
							<li><a href="#">Колонки</a></li>
							<li><a href="#">Усилители</a></li>
							<li><a href="#">Микрофоны</a></li>
							<li><a href="#">Микшеры</a></li>
						</ul>
					</div>
					<div class="sp2">
						<header class="foo-menu__header">
							Видеоотображение информации
						</header>
						<ul class="foo-menu__menu">
							<li><a href="#">ЖК-панели</a></li>
							<li><a href="#">Проекторы</a></li>
							<li><a href="#">Экраны</a></li>
							<li><a href="#">Коммутаторы</a></li>
							<li><a href="#">Коммутация</a></li>
							<li><a href="#">Крепления</a></li>
							<li><a href="#">Лифты и панели</a></li>
						</ul>
					</div>
					<div class="sp2">
						<header class="foo-menu__header">
							Постановочное освещение
						</header>
						<ul class="foo-menu__menu">
							<li><a href="#">Генeраторы</a></li>
							<li><a href="#">Жидкости</a></li>
							<li><a href="#">Конфетти</a></li>
							<li><a href="#">Поворотные головы</a></li>
							<li><a href="#">Прожекторы</a></li>
							<li><a href="#">Лазеры</a></li>
							<li><a href="#">Управление</a></li>
						</ul>
					</div>
					<div class="sp2">
						<header class="foo-menu__header">
							Аксессуары и комплектующие
						</header>
						<ul class="foo-menu__menu">
							<li><a href="#">Проектирование</a></li>
							<li><a href="#">Поставка</a></li>
							<li><a href="#">Монтаж</a></li>
							<li><a href="#">Подключение</a></li>
							<li><a href="#">Настройка</a></li>
							<li><a href="#">Обучение персонала</a></li>
							<li><a href="#">Техническая поддержка</a></li>
							<li><a href="#">Обслуживание</a></li>
						</ul>
					</div>
					<div class="col-xs-12">
						<ul class="foo-menu__service">
							<li><span>Услуги AV</span></li>
							<li><a href="#">Проектирование</a></li>
							<li><a href="#">Поставка</a></li>
							<li><a href="#">Монтаж</a></li>
							<li><a href="#">Подключение</a></li>
							<li><a href="#">Настройка</a></li>
							<li><a href="#">Обучение персонала</a></li>
							<li><a href="#">Техническая поддержка</a></li>
							<li><a href="#">Обслуживание</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container foo-copyright">
			<div class="row">
				<div class="col-xs-6 foo-copyright__copy">
					<p>© 2014-2015 IN/OUT<br>
						 ИП Якунин Руслан Маруанович, УНП 191482637<br>
						 св. № 0261135 от 19 января 2011 г.<br>
						 пр-д Ташкентский 12-17, Минск, Беларусь<br></p>
					<p><span>&copy; 2015, ООО «IN OUT»</span></p>
				</div>
				<div class="col-xs-6">
					<a href="#" class="foo-social sk"></a>
					<a href="#" class="foo-social tw"></a>
					<a href="#" class="foo-social fb"></a>
					<a href="#" class="foo-social in"></a>
				</div>
			</div>
		</div>
	</footer>

	<!-- scroll to top -->
	<div class="to-top"></div>

	<!-- map -->
	<div id="map" style="display: none;">
		<div style="width: 800px;height: 800px;">
			<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=th4M-9Ny9hst122URc6v5ELqNpeBH9f-&width=800&height=800"></script>
		</div>
	</div>

	
	<!-- Scripts -->
	<script src="js/min/jquery-1.11.3-min.js"></script>
	<script src="js/min/slick-min.js"></script>
	<script src="js/min/jquery.fancybox.pack-min.js"></script>
	<script src="js/min/jquery-ui-min.js"></script>
	<script src="js/min/jquery.bxslider-min.js"></script>
	<script src="js/min/jquery.doubleScroll-min.js"></script>
	<script src="js/min/script-min.js"></script>
</body>
</html>