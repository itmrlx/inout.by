<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=980, initial-scale=1">
		<link href="/favicon.png" rel="icon" type="image/png">
		<title>inout.by</title>

		<!-- Styles -->
		<link rel="stylesheet" href="css/global.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/print.css" media="print">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
<body>

	<!-- head -->
	<header class="container head">
		<div class="row">
			<div class="col-xs-6 head__logo">
				<div class="logo">
					<a class="alignleft" href="/"><img class="logo__img" src="img/logo.jpg" alt="inout"></a>
					<p class="head__desc">Системная интеграция звук, свет, видео.<br>
						 Составление полной документации, продажа, установка
						 и настройка светового и звукового оборудования.</p>
					<p class="header-rek">
						Частное предприятие "ИНАУТ"<br>
						Юр. Адрес: 220077, г. Минск, пр-д Ташкентский 12-17<br>
						УНП 192358261 ОКПО 382020605000<br>
						р/с в белорусских рублях 3012092581008<br>
						в ЗАО "Идея Банк"<br>
						Адрес банка: 220034, РБ, г.Минск, ул. З. Бядули д.11 код 755
					</p>
				</div>
			</div>
			<div class="col-xs-3 head__contacts">
				<div class="contacts-phones">
					<p><span>Velcom</span> +375 (29) 611 71 10</p>
					<p><span>Life</span> +375 (25) 611-71-10</p>
					<p><span>Факс</span> +375 (33) 616 71 10</p>
				</div>
			</div>
			<div class="col-xs-3 head__hours">
				<div class="adres">
					<p class="head__adres">Адрес:</p>
					<p class="head__adres">г.Минск, ул Чижевских 172</p>
					<p class="head__hours">Время раюоты:</p>
					<p class="head__hours">09:00 - 17:00 (Пн. - Пт.)</p>
					<p><strong>Эл. Почта:</strong> <a href="mailto:info@inout.by">info@inout.by</a></p>
					<p class="head__map"><a class="fancybox" href="#map">Карта проезда</a></p>
				</div>
			</div>
		</div>
		<div class="row head__search-panel">
			<div class="col-xs-9 head__search">
				<form class="search-form">
					<input class="search-form__input" type="text" placeholder="Поиск" value="">
					<button class="search-btn" type="submit">Найти</button>
<!-- 					<div class="search-results-container">
						<div class="search-results">
							<div class="row search-results__item">
								<div class="col-xs-3">
									<img src="img/content/item.jpg" alt="item-alt">
								</div>
								<div class="col-xs-6">
									<h2><a href="#">Колонки Alto TrueSonic TSSUB12</a></h2>
									<p class="search-results__cost">3 999 999 Br</p>
								</div>
								<div class="col-xs-3">
									<a href="#" class="btn btn-default">Заказать</a>
								</div>
							</div>
							<div class="row search-results__item">
								<div class="col-xs-3">
									<img src="img/content/item.jpg" alt="item-alt">
								</div>
								<div class="col-xs-6">
									<h2><a href="#">Колонки Alto TrueSonic TSSUB12</a></h2>
									<p class="search-results__cost">3 999 999 Br</p>
								</div>
								<div class="col-xs-3">
									<a href="#" class="btn btn-default">Заказать</a>
								</div>
							</div>
							<div class="row search-results__item">
								<div class="col-xs-3">
									<img src="img/content/item.jpg" alt="item-alt">
								</div>
								<div class="col-xs-6">
									<h2><a href="#">Колонки Alto TrueSonic TSSUB12</a></h2>
									<p class="search-results__cost">3 999 999 Br</p>
								</div>
								<div class="col-xs-3">
									<a href="#" class="btn btn-default">Заказать</a>
								</div>
							</div>
						</div>
					</div> -->
				</form>
			</div>
			<div class="col-xs-3 head__cart">
				<a href="#" class="cart-small">
					<p>1 товар на сумму</p>
					<p><span>12 000 000 Br</span></p>
				</a>
			</div>
		</div>
	</header>
	
	<!-- navigation -->
	<div class="wrapper navigation">
		<div class="container">
			<div class="row navigation__first">
				<div class="col-xs-12">
					<nav class="menu-catalog">
						<ul>
							<li class="has-submenu has-catalog"><a href="#">Каталог</a></li>
							<li><a href="#">Бренды</a></li>
							<li class="has-submenu"><a href="#">Покупателю</a>
								<ul class="sub-menu">
									<li><a href="#">Оплата</a></li>
									<li><a href="#">Скидочная карта</a></li>
									<li><a href="#">Доставка</a></li>
									<li><a href="#">Возврат и обмен товара</a></li>
									<li><a href="#">Правила ухода и эксплуатации</a></li>
								</ul>
							</li>
							<li><a href="#">Журнал</a></li>
							<li><a href="#">AV Услуги</a></li>
							<li><a href="#">О компании</a></li>
							<li><a href="#">Дилерам</a></li>
							<li><a href="#">Отзывы</a></li>
							<li class="lc"><a href="#">Личный кабинет</a></li>
						</ul>
						<!-- menu catalog -->
						<div class="sub-menu-catalog">
							<div class="row">
								<div class="col-xs-4 sub-menu-catalog__left">
									<ul>
										<li class="sub-menu-catalog__i1 active"><a href="#">Системы оповещения и трансляции</a></li>
										<li class="sub-menu-catalog__i2"><a href="#">Концертное звукоусиление</a></li>
										<li class="sub-menu-catalog__i3"><a href="#">Видеоотображение информации</a></li>
										<li class="sub-menu-catalog__i4"><a href="#">Постановочное освещение</a></li>
										<li class="sub-menu-catalog__i5"><a href="#">Аксесуары и комплектующие</a></li>
										<li class="sub-menu-catalog__i6"><a href="#">Услуги АV</a></li>
									</ul>
								</div>
								<div class="col-xs-8 sub-sub-menu-catalog" id="sub-menu-catalog__i1">
									<div class="row">
										<div class="col-xs-6">
											<ul>
												<li><a href="#">Громкоговорители <span>14</span></a></li>
												<li><a href="#">Усилители 70/100V <span>214</span></a></li>
												<li><a href="#">Микрофоны настольные <span>14</span></a></li>
												<li><a href="#">Радиомикрофоны <span>14</span></a></li>
												<li><a href="#">Конференц системы <span>14</span></a></li>
												<li><a href="#">Мегафоны <span>14</span></a></li>
												<li><a href="#">Проигрыватели <span>14</span></a></li>
											</ul>
										</div>
										<div class="col-xs-6 sub-sub-menu-catalog__brands">
											<header>Бренды нашей продукции</header>
											<div class="row">
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-8 sub-sub-menu-catalog" id="sub-menu-catalog__i2">
									<div class="row">
										<div class="col-xs-6">
											<ul>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
											</ul>
										</div>
										<div class="col-xs-6 sub-sub-menu-catalog__brands">
											<header>Бренды нашей продукции</header>
											<div class="row">
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-8 sub-sub-menu-catalog" id="sub-menu-catalog__i3">
									<div class="row">
										<div class="col-xs-6">
											<ul>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
											</ul>
										</div>
										<div class="col-xs-6 sub-sub-menu-catalog__brands">
											<header>Бренды нашей продукции</header>
											<div class="row">
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-8 sub-sub-menu-catalog" id="sub-menu-catalog__i4">
									<div class="row">
										<div class="col-xs-6">
											<ul>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
											</ul>
										</div>
										<div class="col-xs-6 sub-sub-menu-catalog__brands">
											<header>Бренды нашей продукции</header>
											<div class="row">
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-8 sub-sub-menu-catalog" id="sub-menu-catalog__i5">
									<div class="row">
										<div class="col-xs-6">
											<ul>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
											</ul>
										</div>
										<div class="col-xs-6 sub-sub-menu-catalog__brands">
											<header>Бренды нашей продукции</header>
											<div class="row">
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-8 sub-sub-menu-catalog" id="sub-menu-catalog__i6">
									<div class="row">
										<div class="col-xs-6">
											<ul>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
												<li><a href="#">Пункт меню</a></li>
											</ul>
										</div>
										<div class="col-xs-6 sub-sub-menu-catalog__brands">
											<header>Бренды нашей продукции</header>
											<div class="row">
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
												<div class="col-xs-6">
													<a href="#"><img src="img/content/logo-bigzz.png" alt="bigzz"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</nav>
				</div>
			</div>
			<div class="row navigation__last">
				<div class="col-xs-12">
					<nav class="menu-items">
						<ul>
							<li><a href="#">Микшеры <span>от 529 000 BR</span></a></li>
							<li><a href="#">Колонки <span>от 529 000 BR</span></a></li>
							<li><a href="#">Микрофоны <span>от 529 000 BR</span></a></li>
							<li><a href="#">Усилители <span>от 529 000 BR</span></a></li>
							<li><a href="#">Генераторы <span>от 529 000 BR</span></a></li>
							<li><a href="#">Жидкости <span>от 529 000 BR</span></a></li>
						</ul> 
					</nav>
				</div>
			</div>
		</div>
	</div>