<?php include 'inc/header.php'; ?>
	<!-- page head -->
	<div class="wrapper img-header"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 breadcrumbs">
				<ul>
					<li><a href="#">Главная</a></li><span>&rsaquo;</span>
					<li><a href="#">Корзина</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- tile -->
	<div class="container">
		<header>
			<h1 class="page-title">Журнал AV</h1>
		</header>
	</div>


	<!-- tabs -->
	<div class="container">
		<div id="journaltabs" class="col-xs-12">
			<ul class="jtabs-container">
				<li><a href="#tabs-1">Все проекты <span>2</span></a></li>
				<li><a href="#tabs-2">Обзоры <span>3</span></a></li>
				<li><a href="#tabs-3">Мы посещаем семинары <span>4</span></a></li>
				<li><a href="#tabs-4">Развлекательные цунтры <span>5</span></a></li>
			</ul>
			<!-- hide block -->
			<div class="journal-slide-block">
				<span class="btn-jslide"></span>
				<div>
					<h3>Мы рады сообщить вам об открытии  журнала аудиовизуальных технологий.</h3>
					<p>
						В этих&nbsp;номерах мы будем&nbsp;опубликовывать рассказы об инсталляциях нашей молодой компании, а так же будем стараться писать интересные материалы по темам касающимся системной интеграции в Республике Беларусь в области аудио видео сектора, а также постановочного освещения.
					</p>
				</div>
				<div class="journal-slide-more" style="display: none;">
					<p>
						Журнал AV "ИНАУТ" будет информировать о современных  и передовых технологиях, а так же вовлекать новых рекламодателей со стороны белорусского рынка.
					</p>
				</div>
			</div>
			<!-- / hide block -->
			<div id="tabs-1">
				<div class="row">
					<!-- items -->
					<div class="col-xs-4 j-item">
						<a href="#" class="j-item__wrap">
							<div class="j-item__img-container" style="border-color:#0097e9;">
								<img src="img/content/jitem.jpg" alt="item">
							</div>
							<img src="img/content/pvt_logo.jpg" alt="logo">
						</a>
						<a href="#">
							<h2 class="j-item__title">
								Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ
							</h2>
						</a>
						<div class="j-item__desc">
							Зал-трансформер для проведения конференций, семинаров, симпозиумов, собраний.
						</div>
						<div>
							<a href="#" class="j-item-btn-video"></a>
							<span class="j-item-view">92</span>
							<span class="j-item-dollars v1"></span>
							<div class="cliarfix"></div>
						</div>
					</div>
					<div class="col-xs-4 j-item">
						<a href="#" class="j-item__wrap">
							<div class="j-item__img-container" style="border-color:#0097e9;">
								<img src="img/content/jitem.jpg" alt="item">
							</div>
							<img src="img/content/pvt_logo.jpg" alt="logo">
						</a>
						<a href="#">
							<h2 class="j-item__title">
								Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ
							</h2>
						</a>
						<div class="j-item__desc">
							Зал-трансформер для проведения конференций, семинаров, симпозиумов, собраний.
						</div>
						<div>
							<a href="#" class="j-item-btn-video"></a>
							<span class="j-item-view">92</span>
							<span class="j-item-dollars v1"></span>
							<div class="cliarfix"></div>
						</div>
					</div>
					<div class="col-xs-4 j-item">
						<a href="#" class="j-item__wrap">
							<div class="j-item__img-container" style="border-color:#0097e9;">
								<img src="img/content/jitem.jpg" alt="item">
							</div>
							<img src="img/content/pvt_logo.jpg" alt="logo">
						</a>
						<a href="#">
							<h2 class="j-item__title">
								Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ
							</h2>
						</a>
						<div class="j-item__desc">
							Зал-трансформер для проведения конференций, семинаров, симпозиумов, собраний.
						</div>
						<div>
							<a href="#" class="j-item-btn-video"></a>
							<span class="j-item-view">92</span>
							<span class="j-item-dollars v1"></span>
							<div class="cliarfix"></div>
						</div>
					</div>
					<div class="col-xs-4 j-item">
						<a href="#" class="j-item__wrap">
							<div class="j-item__img-container" style="border-color:#0097e9;">
								<img src="img/content/jitem.jpg" alt="item">
							</div>
							<img src="img/content/pvt_logo.jpg" alt="logo">
						</a>
						<a href="#">
							<h2 class="j-item__title">
								Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ
							</h2>
						</a>
						<div class="j-item__desc">
							Зал-трансформер для проведения конференций, семинаров, симпозиумов, собраний.
						</div>
						<div>
							<a href="#" class="j-item-btn-video"></a>
							<span class="j-item-view">92</span>
							<span class="j-item-dollars v1"></span>
							<div class="cliarfix"></div>
						</div>
					</div>
					<div class="col-xs-4 j-item">
						<a href="#" class="j-item__wrap">
							<div class="j-item__img-container" style="border-color:#0097e9;">
								<img src="img/content/jitem.jpg" alt="item">
							</div>
							<img src="img/content/pvt_logo.jpg" alt="logo">
						</a>
						<a href="#">
							<h2 class="j-item__title">
								Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ
							</h2>
						</a>
						<div class="j-item__desc">
							Зал-трансформер для проведения конференций, семинаров, симпозиумов, собраний.
						</div>
						<div>
							<a href="#" class="j-item-btn-video"></a>
							<span class="j-item-view">92</span>
							<span class="j-item-dollars v1"></span>
							<div class="cliarfix"></div>
						</div>
					</div>
					<div class="col-xs-4 j-item">
						<a href="#" class="j-item__wrap">
							<div class="j-item__img-container" style="border-color:#0097e9;">
								<img src="img/content/jitem.jpg" alt="item">
							</div>
							<img src="img/content/pvt_logo.jpg" alt="logo">
						</a>
						<a href="#">
							<h2 class="j-item__title">
								Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ
							</h2>
						</a>
						<div class="j-item__desc">
							Зал-трансформер для проведения конференций, семинаров, симпозиумов, собраний.
						</div>
						<div>
							<a href="#" class="j-item-btn-video"></a>
							<span class="j-item-view">92</span>
							<span class="j-item-dollars v1"></span>
							<div class="cliarfix"></div>
						</div>
					</div>
					<!-- / items -->
				</div>
			</div>
			<div id="tabs-2">
				2
			</div>
			<div id="tabs-3">
				3
			</div>
			<div id="tabs-4">
				4
			</div>
		</div>
	</div>

	<!-- form -->
	<div class="container gray-container cart-form j-item-form">
		<form class="form row ajax_form" action="" method="post">
			<input type="hidden" name="nospam:blank" value="">
			<div class="col-xs-8">
				<header>Форма заказа</header>
				<p>Если вас заинтересовали наши услуги напишите нам, мы свяжемся с вами в ближейшее время.</p>
			</div>
			<div class="col-xs-7">
				<label for="af_name">Ваше имя</label>
				<input type="text" name="name" value="" id="af_name">
				<span class="error_name"></span>
			</div>
			<div class="col-xs-7">
				<label for="af_email">Контактная эл. почта</label>
				<input type="text" name="email" value="" id="af_email">
				<span class="error_email"></span>
			</div>
			<div class="col-xs-7">
				<label for="af_message">Кратко опишите заказ</label>
				<textarea type="text" rows="7" id="af_message" name="message"></textarea>
				<span class="error_message"></span>
			</div>
			<div class="col-xs-7">
				<br>
				<button class="btn-default btn-ttu" type="submit">Отправить</button>
			</div>
		</form>
	</div>

<?php include 'inc/footer.php'; ?>