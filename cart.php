<?php include 'inc/header.php'; ?>
	<!-- page head -->
	<div class="wrapper img-header"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-9 breadcrumbs">
				<ul>
					<li><a href="#">Главная</a></li><span>&rsaquo;</span>
					<li><a href="#">Корзина</a></li>
				</ul>
			</div>
			<div class="col-xs-3">
				<a class="back-to-cat-btn"><span>Вернуться в каталог</span></a>
			</div>
		</div>
	</div>

	<!-- tile -->
	<div class="container">
		<header>
			<h1 class="page-title">Корзина</h1>
		</header>
	</div>

	<!-- items -->
	<div class="container">
		<!-- cart items -->
		<div class="row cart-item">
			<div class="col-xs-1">
				<img src="img/content/item.jpg" alt="item">
			</div>
			<div class="col-xs-5 cart-item__column">
				Aктивная колонка Alto TS 110A<br>
				Гарантия: 1 год, Вес: 12.1 кг.
			</div>
			<div class="col-xs-2 cart-item__column">
				<form method="post" class="ms2_form" role="form">
					<input type="hidden" name="key" value="1aa1a48eca3cd856ec2b7b72cbaa0271">
					<a href="#minux" class="minus"></a>
					<input class="num" name="count" type="text" value="6" max-legth="4">
					<a href="#plus" class="plus"></a>
					<button type="submit" name="ms2_action" value="cart/change" style="display: none;"></button>
				</form>
			</div>
			<div class="col-xs-3 cart-item__column cart-item__cost">
				39 426 000 Br
			</div>
			<div class="col-xs-1 cart-item__column">
				<form method="post" class="ms2_form">
					<input type="hidden" name="key" value="1aa1a48eca3cd856ec2b7b72cbaa0271">			
					<button class="del" type="submit" name="ms2_action" value="cart/remove" title="Удалить"></button>
				</form>
			</div>
		</div>
		<div class="row cart-item">
			<div class="col-xs-1">
				<img src="img/content/item.jpg" alt="item">
			</div>
			<div class="col-xs-5 cart-item__column">
				Aктивная колонка Alto TS 110A<br>
				Гарантия: 1 год, Вес: 12.1 кг.
			</div>
			<div class="col-xs-2 cart-item__column">
				<form method="post" class="ms2_form" role="form">
					<input type="hidden" name="key" value="1aa1a48eca3cd856ec2b7b72cbaa0271">
					<a href="#minux" class="minus"></a>
					<input class="num" name="count" type="text" value="6" max-legth="4">
					<a href="#plus" class="plus"></a>
					<button type="submit" name="ms2_action" value="cart/change" style="display: none;"></button>
				</form>
			</div>
			<div class="col-xs-3 cart-item__column cart-item__cost">
				39 426 000 Br
			</div>
			<div class="col-xs-1 cart-item__column">
				<form method="post" class="ms2_form">
					<input type="hidden" name="key" value="1aa1a48eca3cd856ec2b7b72cbaa0271">			
					<button class="del" type="submit" name="ms2_action" value="cart/remove" title="Удалить"></button>
				</form>
			</div>
		</div>
		<div class="row cart-item">
			<div class="col-xs-1">
				<img src="img/content/item.jpg" alt="item">
			</div>
			<div class="col-xs-5 cart-item__column">
				Aктивная колонка Alto TS 110A<br>
				Гарантия: 1 год, Вес: 12.1 кг.
			</div>
			<div class="col-xs-2 cart-item__column">
				<form method="post" class="ms2_form" role="form">
					<input type="hidden" name="key" value="1aa1a48eca3cd856ec2b7b72cbaa0271">
					<a href="#minux" class="minus"></a>
					<input class="num" name="count" type="text" value="6" max-legth="4">
					<a href="#plus" class="plus"></a>
					<button type="submit" name="ms2_action" value="cart/change" style="display: none;"></button>
				</form>
			</div>
			<div class="col-xs-3 cart-item__column cart-item__cost">
				39 426 000 Br
			</div>
			<div class="col-xs-1 cart-item__column">
				<form method="post" class="ms2_form">
					<input type="hidden" name="key" value="1aa1a48eca3cd856ec2b7b72cbaa0271">			
					<button class="del" type="submit" name="ms2_action" value="cart/remove" title="Удалить"></button>
				</form>
			</div>
		</div>
		<!-- / cart items -->
	</div>

	<!-- tabs -->
	<div class="container">
		<header>
			<h3><strong>Способ оплаты</strong></h3>
		</header>
		<div id="carttabs" class="col-xs-12">
			<ul>
				<li><a href="#tabs-1">Наличными</a></li>
				<li><a href="#tabs-2">Безналичными</a></li>
				<li><a href="#tabs-3">Карточкой</a></li>
			</ul>
			<div id="tabs-1">
				<div id="deliveries" class="cd-form">
					<div class="radio-btns " data-default-city="Минск" style="display: block;">
						<input class="delivery-input " type="radio" name="delivery" value="3" id="delivery_3" checked="">
						<label for="delivery_3">Доставка по Минску (бесплатно)</label>
						<div class="desc" style="display: none;"></div>    
					</div>
					<div class="radio-btns radio-maps" data-default-city="Минск" style="display: block;">
						<input class="delivery-input samovivoz" type="radio" name="delivery" value="1" id="delivery_1">
						<label for="delivery_1">Самовывоз со склада в Минске (пн–пт с 9:00 – 17:00)</label>
						<a href="#map" class="fancybox">Минск, ул. Чижевских 172 (карта)</a>
						<div class="desc" style="display: none;"><p><strong>Внимание!</strong> Прежде чем ехать за товаром, обязательно дождитесь подтверждения заказа от нашего менеджера</p></div>    
					</div>
					<div class="radio-btns " data-default-city="" style="display: block;">
						<input class="delivery-input " type="radio" name="delivery" value="4" id="delivery_4">
						<label for="delivery_4">Доставка по Беларуси</label>
						<div class="desc" style="display: none;"></div>
					</div>
					<div class="delivery-message" style="display: block;">
						<i><strong>Внимание!</strong> Прежде чем ехать за товаром, обязательно<br>
							дождитесь подтверждения заказа от нашего менеджера</i>
					</div>
				</div>
				<div class="container gray-container cart-form">
					<!-- form 1 -->
						<div class="form row">
							<div class="col-xs-7">
								<div class="row">
									<div class="col-xs-9 form-header">
										<header>Адрес доставки</header>
									</div>
									<div class="col-xs-9">
										<label for="city">Город</label>
										<div class="tip-block">
											<input type="text" id="city" placeholder="Минск" name="city" value="Минск" class="">
											<div class="tip tip-blue" style="display: block;">
												Доставка по Минску<br>осуществляется бесплатно.
											</div>
										</div>
									</div>
									<div class="col-xs-9">
										<label for="street">Улица, Дом, Квартира</label>
										<div class="tip-block">                        
											<input type="text" id="street" placeholder="п-кт Победителей, д. 18, офис 3412" name="street" value="" class="">
										</div>
									</div>
									<div class="col-xs-9">
										<header>Контактные данные</header>
									</div>
									<div class="col-xs-9">
										<label for="receiver">Ваше имя</label>
										<div class="tip-block">
											<input type="text" id="receiver" placeholder="Иван Иванов" name="receiver" value="" class="">
											<div class="tip tip-red small-tip" style="display: block;">
												Обязательное поле
											</div>
										</div>
									</div>
									<div class="col-xs-9">
										<label>Контактный телефон</label>
										<div class="tip-block">
											<input type="text" id="phone" placeholder="+375 33 600 10 20" name="phone" value="" class="">
											<div class="tip tip-blue"></div>
										</div>
									</div>
									<div class="col-xs-9">
										<label>Адрес эл. почты</label>
										<div class="tip-block">
											<input type="email" id="email" placeholder="email@website.com" name="email" value="" class="">
										</div>
									</div>
									<div class="col-xs-9">
										<label>Комментарий к заказу <span>(не обязательно)</span></label>
										<div class="tip-block">                        
											<textarea name="comment" id="comment" cols="30" rows="7" class=""></textarea>
										</div>
									</div>
									<div class="col-xs-9 result">
										<p>Количество: <small class="ocart ms2_total_count" data-source="tcount">9</small> шт. Вес: <small class="ocart ms2_total_weight" data-source="tweight">72.6</small> кг<br>
										Стоимость товара: <small class="ocart ms2_total_cost" data-source="tcost">52 439 000</small> Br<br>
										<span>Итого, с доставкой: <strong class="ms2_order_cost">52 439 000</strong> Br</span></p>
									</div>
									<div class="col-xs-9">
										<button type="submit" name="ms2_action" value="order/submit" class="btn-default btn-ttu">Сделать заказ</button>
									</div>
								</div>
							</div>
							<div class="col-xs-5">
								<div class="small-5 delivery" id="beldelivery" style="display: block;">
									<div class="beznal-desc">
										<p>Стоимость доставки уточняйте у менеджера после оформления заказа.</p>
										<p>Телефон: <strong>+375 (29) 285 15 83</strong></p>
										<p>Эл. почта: <a href="mailto:info@inout.by">info@inout.by</a></p>
									</div>
								</div>
							</div>
						</div>
					<!-- / form 1 -->
				</div>
			</div>
			<div id="tabs-2">
				<div class="small-12 column beznal-desc beznal-desc2">
					<p>График работы отдела корпоративных продаж: пн–пт 9:00 – 17:00<br>После обработки заказа счет будет выслан на ваш адрес эл. почты.</p>
					<p>Если вы не получили счет или у вас возникли вопросы, связанные с оформлением<br>данного заказа, пожалуйста, обращайтесь к нам:</p>
					<p>Телефон: <strong>+375 (29) 285 15 83</strong></p>
					<p>Эл. почта: <a href="mailto:info@inout.by">info@inout.by</a></p>
				</div>
				<!-- form 2 -->
				<div class="gray-container">
					<div class="row cart-form cart-form-2">
						<div class="col-xs-4">
								<div class="col-xs-12 form-header">
										<header class="header-form-two">Информация об организации</header>
								</div>
								<div class="col-xs-12">
										<label for="company">Юридическое название</label>
										<div class="tip-block">
												<input type="text" id="company" placeholder="ООО Иванов и Со" name="company" value="" class="">
												<div class="tip tip-red small-tip">
														Обязательное поле
												</div>
										</div>
								</div>
								<div class="col-xs-12">
										<label for="legaladdress">Юридический адрес</label>
										<div class="tip-block">
												<input type="text" id="legaladdress" placeholder="Минск, п-кт Победителей, 40, оф. 22" name="legaladdress" value="" class="">
												<div class="tip tip-red small-tip">
														Обязательное поле
												</div>
										</div>
								</div>
								<div class="col-xs-12">
										<label for="account">Расчетный счет</label>
										<div class="tip-block">
												<input type="text" id="account" placeholder="000 000 000 0000" name="account" value="" class="">
												<div class="tip tip-red small-tip">
														Обязательное поле
												</div>
										</div>
								</div>
								<div class="col-xs-12">
										<label for="bank">Банк</label>
										<div class="tip-block">
												<input type="text" id="bank" placeholder="ОАО Приорбанк" name="bank" value="" class="">
												<div class="tip tip-red small-tip">
														Обязательное поле
												</div>
										</div>
								</div>
								<div class="col-xs-12">
										<label for="code">Код банка</label>
										<div class="tip-block">
												<input type="text" id="code" placeholder="749" name="code" value="" class="">
												<div class="tip tip-red small-tip">
														Обязательное поле
												</div>
										</div>
								</div>
								<div class="col-xs-12">
										<label for="apn">УНП</label>
										<div class="tip-block">
												<input type="text" id="apn" placeholder="000000000" name="apn" value="" class="">
												<div class="tip tip-red small-tip">
														Обязательное поле
												</div>
										</div>
								</div>
								<div class="col-xs-12">
										<label for="ceo">Директор, ФИО</label>
										<div class="tip-block">
												<input type="text" id="ceo" placeholder="Иванов И.И." name="ceo" value="" class="">
												<div class="tip tip-red small-tip">
														Обязательное поле
												</div>
										</div>
								</div>
								<div class="col-xs-12">
										<label for="based">На основании</label>
										<div class="tip-block">
												<input type="text" id="based" placeholder="Устав" name="based" value="" class="">
												<div class="tip tip-red small-tip">
														Обязательное поле
												</div>
										</div>
								</div>
						</div>
						<div class="col-xs-4 col-xs-offset-1 column end">
								<div class="small-12 form-header">
										<header class="header-form-two">Контактные данные</header>
								</div>                            
								<div class="col-xs-12">
										<label for="receiver">Ваше имя</label>
										<div class="tip-block">
												<input type="text" id="receiver" placeholder="Иван Иванов" name="receiver" value="" class="">
												<div class="tip tip-red small-tip">
														Обязательное поле
												</div>
										</div>
								</div>
								<div class="col-xs-12">
										<label for="phone">Контактный телефон</label>
										<div class="tip-block">
												<input type="text" id="phone" placeholder="+375 33 600 10 20" name="phone" value="" class="">
												<div class="tip tip-red small-tip">
														Обязательное поле
												</div>
												<div class="tip tip-blue"></div>
										</div>
								</div>
								<div class="col-xs-12">
										<label for="email">Адрес эл. почты</label>
										<div class="tip-block">
												<input type="text" id="email" placeholder="email@website.com" name="email" value="" class="">
												<div class="tip tip-red small-tip">
														Обязательное поле
												</div>
										</div>
								</div>
								<div class="col-xs-12">
										<label for="comment">Комментарий к заказу<br><span>(не обязательно)</span></label>
										<div class="tip-block">
												<textarea name="comment" id="comment" cols="30" rows="7" class=""></textarea>
										</div>
								</div>
						</div>
					</div>
				</div>
				<!-- / form 2 -->
			</div>
			<div id="tabs-3">
				<p>Сейчас оплата картами недоступна по техническим причинам, но мы работаем над этим и скоро можно будет оплатить картами.</p><br><br><br>
			</div>
		</div>
	</div>

<?php include 'inc/footer.php'; ?>