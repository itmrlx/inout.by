<?php include 'inc/header.php'; ?>

	<!-- slider -->
	<div class="wrapper slider">
		<div class="container slick-main">
				<div class="slider__slide" style="background-image: url(img/slides/1.png);">
					<div class="col-xs-9">
						<h2 class="slider__header">Проекционное оборудование, системы видео-отображения</h2>
						<p class="slider__description">Модульные конструкции, удобство<br>обслуживания и эксплуатации</p>
						<a href="#" class="btn btn-default btn-default-upper">Заказать консультацию</a>
					</div>
					<div class="col-xs-3">
						<div class="slider-cost">
							<p class="slider-cost__top">от <span>$906</span></p>
							<p class="slider-cost__bottom">инсталяция от <span>$950</span></p>
						</div>
					</div>
				</div>
				<div class="slider__slide" style="background-image: url(img/slides/2.png);">
					<div class="col-xs-9">
						<h2 class="slider__header">Профессиональное <br>звуковое и световое <br>оборудование</h2>
						<p class="slider__description">На всю линейку светодиодных прожекторов <br>действует гарантия</p>
						<a href="#" class="btn btn-default btn-default-upper">Заказать консультацию</a>
					</div>
					<div class="col-xs-3">
						<div class="slider-cost">
							<p class="slider-cost__top">от <span>$999</span></p>
							<p class="slider-cost__bottom">инсталяция от <span>$150</span></p>
						</div>
					</div>
				</div>
		</div>
	</div>

	<!-- services -->
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="main-title">Услуги нашей компании</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4">
				<div class="main-service">
					<a href="#" class="main-service__title main-service__t1">
						<span>Проектирование</span>
					</a>
					<div class="main-service__desc">
						Подготовка полной технической документации по проекту.
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="main-service">
					<a href="#" class="main-service__title main-service__t2">
						<span>Поставка</span>
					</a>
					<div class="main-service__desc">
						Собственная дистрибуторский компания, отличная логистика, сжаты сроки поставки.
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="main-service">
					<a href="#" class="main-service__title main-service__t3">
						<span>Монтаж</span>
					</a>
					<div class="main-service__desc">
						Комплексные услуги, закладка кабелей на стадии строительства.
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="main-service">
					<a href="#" class="main-service__title main-service__t4">
						<span>Подключение</span>
					</a>
					<div class="main-service__desc">
						Пусконаладка слаботочный систем, тестирование и проверка.
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="main-service">
					<a href="#" class="main-service__title main-service__t5">
						<span>Настройка</span>
					</a>
					<div class="main-service__desc">
						Запуск и интеграция возможностей оборудования, высокий уровень специалистов.
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="main-service">
					<a href="#" class="main-service__title main-service__t6">
						<span>Обучение персонала</span>
					</a>
					<div class="main-service__desc">
						Творческий подход, упрощенные схемы включения оборудования, положительные отзывы клиентов.
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="main-service">
					<a href="#" class="main-service__title main-service__t7">
						<span>Техническая поддержка</span>
					</a>
					<div class="main-service__desc">
						Выезд специалистов, помощь и консультации, обслуживание вашего оборудования.
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- distributors -->
	<div class="wrrapper wr-blue distributors">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="main-title">Мы являемся официальным дистрибъютером в РБ</div>
				</div>
			</div>
			<div class="row distributors__distrib">
				<div class="col-xs-2"><img src="img/distributors/1.png" alt="fonestar"></div>
				<div class="col-xs-4">
					Fоnestar — трансляционное оборудование, конференц системы, системы оповещения.<br>
					<a href="#">Подтверждение эксклюзивного представителя</a>
				</div>
				<div class="col-xs-2"><img src="img/distributors/4.png" alt="antari"></div>
				<div class="col-xs-4">
					Antari — генераторы дыма, тумана,снега, мыльных пузырей, жидкости к ним.
				</div>
			</div>
			<div class="row distributors__distrib">
				<div class="col-xs-2"><img src="img/distributors/2.png" alt="fonestar"></div>
				<div class="col-xs-4">
					INTER M — звуковое оборудование, системы оповещения и трансляции. Конференц системы.
				</div>
				<div class="col-xs-2"><img src="img/distributors/5.png" alt="antari"></div>
				<div class="col-xs-4">
					Universal effect — профессиональные жидкости дыма, тумана, снега, пены, мыльных пузырей, генераторы к ним.<br>
					<a href="#">Подтверждение эксклюзивного представителя</a>
				</div>
			</div>
			<div class="row distributors__distrib">
				<div class="col-xs-2"><img src="img/distributors/3.png" alt="fonestar"></div>
				<div class="col-xs-4">
					Barco — надежные проекторы для бизнеса, лампы и аксессуары к ним, беспроводные технологии передачи данных.
				</div>
				<div class="col-xs-2"><img src="img/distributors/6.png" alt="antari"></div>
				<div class="col-xs-4">
					Roxtone — кабельная продукция, профессиональные разъемы для различных сигналов.
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="main-title2">Приглашаем местных и региональных дилеров<br>к сотрудничеству.</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6 distributors__item">
					<p>Мы хотим предложить весь спектр услуг своим партнерам , благодаря многолетнему опыту и сбалансированному развитию трех основных направления деятельности компании – дистрибуция , инсталляция и аренда.</p>
					<p>Наши партнеры всегда могут напрямую обратиться как к менеджерам, так и к руководству компании. Мы постоянно изучаем потребности рынка и неизменно дополняем и хотим улучшать ассортимент своей продукции.</p>
				</div>
				<div class="col-xs-6 distributors__item">
					<h5 class="distributors__title">Основные преимущества работы с нами:</h5>
					<ul>
						<li>Конкурентоспособные цены</li>
						<li>Широкий ассортимент товаров</li>
						<li>Удобные индивидуальные схемы оплаты</li>
						<li>Оперативную доставку своей продукции по Беларуси</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="centered">
						<a href="#" class="btn btn-default">Начать сотрудничество</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- last items -->
	<div class="container last-items">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="main-title">Последние поступления</h2>
			</div>
			<div class="col-xs-3">
				<div class="item">
					<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
					<a href="#"><img src="img/content/item.jpg" alt="item"></a> <!-- link item -->
					<p class="item__date">Неделю назад</p>
					<h2 class="item__title"><a href="#">Universal Effect ST-Smoke Fluid LIGHT</a></h2> <!-- link item -->
					<p class="item__cost">2 265 000 Br</p>
					<p class="item__description">Генератор дыма</p>
					<p class="item__characters">800 Вт / бак 0.8 л / расход 0.8 л - 37 мин / пульт</p>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="item">
					<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
					<a href="#"><img src="img/content/item.jpg" alt="item"></a> <!-- link item -->
					<p class="item__date">Неделю назад</p>
					<h2 class="item__title"><a href="#">Universal Effect ST-Smoke Fluid LIGHT</a></h2> <!-- link item -->
					<p class="item__cost">2 265 000 Br</p>
					<p class="item__description">Генератор дыма</p>
					<p class="item__characters">800 Вт / бак 0.8 л / расход 0.8 л - 37 мин / пульт</p>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="item">
					<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
					<a href="#"><img src="img/content/item.jpg" alt="item"></a> <!-- link item -->
					<p class="item__date">Неделю назад</p>
					<h2 class="item__title"><a href="#">Universal Effect ST-Smoke Fluid LIGHT</a></h2> <!-- link item -->
					<p class="item__cost">2 265 000 Br</p>
					<p class="item__description">Генератор дыма</p>
					<p class="item__characters">800 Вт / бак 0.8 л / расход 0.8 л - 37 мин / пульт</p>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="item">
					<a href="#" class="item__addto"><span>Добавить в сравнение</span></a>
					<a href="#"><img src="img/content/item.jpg" alt="item"></a> <!-- link item -->
					<p class="item__date">Неделю назад</p>
					<h2 class="item__title"><a href="#">Universal Effect ST-Smoke Fluid LIGHT</a></h2> <!-- link item -->
					<p class="item__cost">2 265 000 Br</p>
					<p class="item__description">Генератор дыма</p>
					<p class="item__characters">800 Вт / бак 0.8 л / расход 0.8 л - 37 мин / пульт</p>
				</div>
			</div>
			<div class="col-xs-12 last-items__button">
				<div class="centered">
					<a href="#" class="btn btn-default">Показать весь каталог</a>
				</div>
			</div>
		</div>
	</div>

	<!-- system integration -->
	<div class="container system-integtation">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="main-title">Системная интеграция</h2>
			</div>
		</div>
		<div class="row system-integtation__row">
			<a href="#" class="col-xs-4 system-integtation__item">
				<img src="img/content/integration1.png" alt="Конференц-залы">
				<header class="system-integtation__header">Конференц-залы</header>
				<ul>
					<li>конференц системы</li>
					<li>системы синхро-перевода</li>
					<li>системы видеоотбражения</li>
				</ul>
			</a>
			<a href="#" class="col-xs-4 system-integtation__item">
				<img src="img/content/integration2.png" alt="Рестораны">
				<header class="system-integtation__header">Рестораны</header>
				<ul>
					<li>настенные громкоговорители</li>
					<li>потолочные громкоговорители</li>
					<li>системы звукоусиления</li>
					<li>системы видеоотбражения</li>
				</ul>
			</a>
			<a href="#" class="col-xs-4 system-integtation__item">
				<img src="img/content/integration3.png" alt="Развлекательные торговые и центры">
				<header class="system-integtation__header">Развлекательные торговые и центры</header>
				<ul>
					<li>трансляционное оборудование</li>
					<li>cистемы оповещения</li>
					<li>cистемы видеоотбражения</li>
				</ul>
			</a>
		</div>
		<div class="row system-integtation__row">
			<a href="#" class="col-xs-4 system-integtation__item">
				<img src="img/content/integration4.png" alt="Гипермаркеты">
				<header class="system-integtation__header">Гипермаркеты</header>
				<ul>
					<li>трансляционное оборудование</li>
					<li>системы оповещения</li>
					<li>системы видеоотбражения</li>
				</ul>
			</a>
			<a href="#" class="col-xs-4 system-integtation__item">
				<img src="img/content/integration5.png" alt="Актовые залы">
				<header class="system-integtation__header">Актовые залы</header>
				<ul>
					<li>звуковое оборудование</li>
					<li>видео оборудование</li>
					<li>световое оборудование</li>
					<li>коммутаторы и коммутация</li>
				</ul>
			</a>
			<a href="#" class="col-xs-4 system-integtation__item">
				<img src="img/content/integration6.png" alt="Ночные клубы">
				<header class="system-integtation__header">Ночные клубы</header>
				<ul>
					<li>генераторы спецэффектов</li>
					<li>жидкости ирасходные материалы</li>
					<li>звуковое оборудование</li>
					<li>световое оборудование</li>
					<li>видео оборудование</li>
				</ul>
			</a>
		</div>
	</div>

	<!-- advantages -->
	<div class="wrapper wr-blue">
		<div class="container advantages">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="main-title">Наши преимущества</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4">
					<header class="advantages__title">Бесплатная консультация</header>
					<p>Бесплатный выезд на объект, консультации и помощь в подборе оборудования</p>
				</div>
				<div class="col-xs-4">
					<header class="advantages__title">Разные спецификации</header>
					<p>Расчет спецификации оборудования в 2-3 вариантах</p>
				</div>
				<div class="col-xs-4">
					<header class="advantages__title">Широкий выбор брендов</header>
					<p>Широкий портфель предлагаемого оборудования ведущих мировых производителей</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4">
					<header class="advantages__title">Опыт работы в сфере 10 лет</header>
					<p>Опытные и квалифицированные мастера, гарантия долгой службы</p>
				</div>
				<div class="col-xs-4">
					<header class="advantages__title">Проекты любой сложности</header>
					<p>Возможность заказа ТЗ и разработки сложных проектов</p>
				</div>
				<div class="col-xs-4">
					<header class="advantages__title">Делаем то что вы ожидаете</header>
					<p>100% результат соответствующим ожидания заказчика</p>
				</div>
			</div>
		</div>
	</div>

	<!-- journal -->
	<div class="container main-journal">
		<div class="row">
			<div class="col-xs-12">
				<div class="main-title">Журнал «IN / OUT»</div>
			</div>
		</div>
		<div class="row main-journal__row">
			<div class="col-xs-12 main-journal-item" style="background-image: url(img/content/journal1.png);">
				<h3 class="main-journal-item__title">Фирменный магазин Life:)</h3>
				<a href="#" class="main-journal-item__link"></a>
				<div class="main-journal-item__characters">
					<a href="#" class="main-journal-item__video"></a>
					<div class="main-journal-item__watch">980</div>
					<a href="#" class="dollars"></a>
				</div>
			</div>
			<div class="col-xs-4 main-journal-item" style="background-image: url(img/content/journal2.png);">
				<h3 class="main-journal-item__title">Фирменный магазин Life:)</h3>
				<a href="#" class="main-journal-item__link"></a>
				<div class="main-journal-item__characters">
					<a href="#" class="main-journal-item__video"></a>
					<div class="main-journal-item__watch">980</div>
					<a href="#" class="dollars"></a>
				</div>
			</div>
			<div class="col-xs-4 main-journal-item" style="background-image: url(img/content/journal3.png);">
				<h3 class="main-journal-item__title">Фирменный магазин Life:)</h3>
				<a href="#" class="main-journal-item__link"></a>
				<div class="main-journal-item__characters">
					<a href="#" class="main-journal-item__video"></a>
					<div class="main-journal-item__watch">980</div>
					<a href="#" class="dollars"></a>
				</div>
			</div>
			<div class="col-xs-4 main-journal-item" style="background-image: url(img/content/journal4.png);">
				<h3 class="main-journal-item__title">Фирменный магазин Life:)</h3>
				<a href="#" class="main-journal-item__link"></a>
				<div class="main-journal-item__characters">
					<a href="#" class="main-journal-item__video"></a>
					<div class="main-journal-item__watch">980</div>
					<a href="#" class="dollars"></a>
				</div>
			</div>
			<div class="col-xs-8">
				<div class="row">
					<div class="col-xs-12 main-journal-item" style="background-image: url(img/content/journal1.png);">
						<h3 class="main-journal-item__title">Фирменный магазин Life:)</h3>
						<a href="#" class="main-journal-item__link"></a>
						<div class="main-journal-item__characters">
							<a href="#" class="main-journal-item__video"></a>
							<div class="main-journal-item__watch">980</div>
							<a href="#" class="dollars"></a>
						</div>
					</div>
					<div class="col-xs-6 main-journal-item" style="background-image: url(img/content/journal2.png);">
						<h3 class="main-journal-item__title">Фирменный магазин Life:)</h3>
						<a href="#" class="main-journal-item__link"></a>
						<div class="main-journal-item__characters">
							<a href="#" class="main-journal-item__video"></a>
							<div class="main-journal-item__watch">980</div>
							<a href="#" class="dollars"></a>
						</div>
					</div>
					<div class="col-xs-6 main-journal-item" style="background-image: url(img/content/journal2.png);">
						<h3 class="main-journal-item__title">Фирменный магазин Life:)</h3>
						<a href="#" class="main-journal-item__link"></a>
						<div class="main-journal-item__characters">
							<a href="#" class="main-journal-item__video"></a>
							<div class="main-journal-item__watch">980</div>
							<a href="#" class="dollars"></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-4 main-journal-item main-journal-item-big" style="background-image: url(img/content/journal5.png);">
				<h3 class="main-journal-item__title">Фирменный магазин Life:)</h3>
				<a href="#" class="main-journal-item__link"></a>
				<div class="main-journal-item__characters">
					<a href="#" class="main-journal-item__video"></a>
					<div class="main-journal-item__watch">980</div>
					<a href="#" class="dollars"></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="centered">
					<div class="btn btn-default">Показать весь журнал</div>
				</div>
			</div>
		</div>
	</div>

<?php include 'inc/footer.php'; ?>