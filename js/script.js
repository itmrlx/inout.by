// main slider
$('.slick-main').slick({
  dots: true,
  infinite: true,
  speed: 500,
  cssEase: 'linear',
  autoplay: true,
  autoplaySpeed: 3000,
  swipe: false
});

// footer slider
$('.footer-slider').slick({
  dots: false,
  infinite: true,
  speed: 500,
  cssEase: 'linear',
  autoplay: true,
  autoplaySpeed: 3000,
  swipe: false,
  slidesToShow: 7,
  slidesToScroll: 1
});

//catalog-menu
$(function () {
  var indexStart = $('.sub-menu-catalog__left li.active').index()+1;
  $('#sub-menu-catalog__i'+indexStart).show();
});
$('.has-catalog').hover(
  function () {
    $('.sub-menu-catalog').stop(true, true).slideDown();
    $('.sub-sub-menu-catalog>.row').css('min-height', $('.sub-menu-catalog__left').height());
  },
  function () {
    if($('.sub-menu-catalog').is(':hover') == false){
      $('.sub-menu-catalog').slideUp();
    }
  }
);
$('.sub-menu-catalog').hover(
  function () {
    $('.sub-menu-catalog').stop(true, true).show(0);
  },
  function () {
    if($('.has-catalog').is(':hover') == false){
      $('.sub-menu-catalog').slideUp();
    }
  }
);
$('.sub-menu-catalog__left li').hover(
  function () {
    var index = $(this).index()+1;
    $('.sub-menu-catalog__left li').removeClass('active');
    $(this).addClass('active');
    $('.sub-sub-menu-catalog').hide();
    $('#sub-menu-catalog__i' + index).show();
  }
);

// menu
$('li.has-submenu').hover(function () {
  $(this).children('.sub-menu').stop(true, false).slideToggle();
});


//scroll to top
$(document).ready(function(){
  if($(window).width() > 1200){
    $(window).scroll(function(){if ($(this).scrollTop() > 300){$('.to-top').fadeIn();}else{$('.to-top').fadeOut();}});
    $('.to-top').click(function () {$('body,html').animate({scrollTop: 0}, 400); return false;});
  }

  initProductnumber();

  $('input[name="phone"]').on('focusout', function() {    
    var phone = $(this).val().replace('+','');
    formatted = '+' + phone.substr(0, 3) + ' (' + phone.substr(3, 2) + ') ' + phone.substr(5,3) + '-' + phone.substr(8,2) + '-' + phone.substr(10,2);
    
    var html = 'Как мы это поняли<br><span>'+ formatted +'</span>';
    $(this).siblings('.tip-blue').html(html).show();
  });

  $('input[name="city"]').on('focusout', function() {
    if ($(this).val().match(/Минск/gi)) {
      $(this).siblings('.tip-blue').show();
    } else {
      $(this).siblings('.tip-blue').hide();
    }
  });

  if ($('.nal').hasClass('ui-tabs-active')) {
    $('.radio-btns').show();
    
    var input = $('.delivery-input:checked');
    $('.delivery-message').html(input.siblings('.desc').html()).show();
    $('#naldelivery').val(input.val());
    if (input.val() == 3) { // по минску
      $('#city').attr('readonly', true);
    } else {
      $('#city').attr('readonly', false);
    }
  } else {
    $('.radio-btns').hide();
  };  

  $('.radio-btns').click(function() {
    $('#city').siblings('.tip-blue').hide();

    $('.delivery-message').hide();
    var desc = $(this).children('.desc').html();
    $('.delivery-message').html(desc).show();

    var city = $(this).data('default-city');
    $('#city').val(city);   

    if ($(this).children('.delivery-input').val() == 3) { // по минску
      $('#city').attr('readonly', true);
    } else {
      $('#city').attr('readonly', false);
    }

    $('#naldelivery').val($(this).children('.delivery-input').val());

    if ($(this).children('.delivery-input').val() == 4) { // по рб
      $('#beldelivery').css({display: 'inline-block'});
    } else {
      $('#beldelivery').hide();
    }

  });

  $('.item-cnt .num').on('change', function(){
    var count = $(this).val();
    var $sum = $(this).parents('.item-cnt').siblings('.cost');    
    var sum = miniShop2.Utils.formatPrice(count * $sum.data('price'));    
    $sum.find('strong').html(sum);
  });

  // product
  $( "#tabs-product" ).tabs({
    active: 0
  });
  $( ".review-cnt a" ).on('click', function() {
    $( "#tabs-product" ).tabs({ active: 2 });
  });

  // magazine
  $("#tabs-journal ul li a").each(function() {
      $(this).attr("href", location.href.toString() + $(this).attr("href"));
  });
  $( "#tabs-journal" ).tabs();
  
  // cart
  $(".tabs-cart ul li a").each(function() {
      $(this).attr("href", location.href.toString() + $(this).attr("href"));
  });

  $( ".tabs-cart" ).tabs({
    activate: function() {
      if($('.nal').hasClass('ui-tabs-active')){
        $('.radio-btns').show();

        var input = $('.delivery-input:checked');
        $('.delivery-message').html(input.siblings('.desc').html()).show();
        $('#naldelivery').val(input.val());

        if (input.val() == 3) { // по минску //disabled
          $('#city').attr('readonly', true);
        } else {
          $('#city').attr('readonly', false);
        }

      } else {
        $('.radio-btns').hide();
        $('.delivery-message').hide();
      };
    }
  });
  
  $('.ocart').each(function() {
    var source = $(this).data('source');
    $(this).html($('#' + source).html());
  });
  
  $('.btn-jslide').click(function() {
    $(this).parent().find('.journal-slide-more').slideToggle();
    $(this).toggleClass('active');
    return false;
  });

    $('.image-link').magnificPopup({
        type:'image'
    });

    function pluralize(number, titles) {
        var cases = [2, 0, 1, 1, 1, 2];
        return titles[ (number%100 > 4 && number%100 < 20) ? 2 : cases[(number%10 < 5) ? number%10 : 5] ];
    }

    // limit filters
    $('.filter').each(function (i, e) {
      var checkboxes = $(e).find('.checkbox-style');
      if (checkboxes.length > 5) {
        // rest of list hide to popup
        var rest = checkboxes.length - 5;
        var desc = ["Еще", rest, pluralize(rest, ["вариант", "варианта", "вариантов"])].join(' ');
        var $button = $('<div class="all-variants"><span>' + desc + '</span></div>');
        $button.insertAfter(e);
        //var modalId = $(e).prop('id') + '_modal';
        var $modal = $('<div class="all-variants-modal"></div>');
        $modal.insertAfter($button);
        $(e).find('.checkbox-style:gt(4)').appendTo($modal);

        // bind events
        $($button).click(function () {
          $(this).next($modal).toggleClass('show').css({top: $(e).find('h2').position().top});
          $(this).toggleClass('show');
        });
      }
    });
});

// fancybox
// create & show titles
jQuery.fn.getTitle = function() { // Copy the title of every IMG tag and add it to its parent A so that fancybox can show titles
  var arr = jQuery("a.fancybox");
  jQuery.each(arr, function() {
    var title = jQuery(this).children("img").attr("title");
    jQuery(this).attr('title',title);
  })
}

// Find a>img and create fancybox image gallery
var thumbnails = jQuery("a:has(img)").not(".nolightbox").filter( function() { return /\.(jpe?g|png|gif|bmp)$/i.test(jQuery(this).attr('href')) });

//find post>a>img
var posts = jQuery(".post"); 
posts.each(function() {
  jQuery(this).find(thumbnails).addClass("fancybox").attr("rel","fancybox"+posts.index(this)).getTitle()
});

// fancybox on
jQuery("a.fancybox").fancybox({
  helpers : {
    overlay : {
      locked : false // try changing to true and scrolling around the page
    }
  },
  padding: 0
});


// cart tabs
$(function() {
  $( "#carttabs" ).tabs({active: 0});
});

// journal tabs
$(function() {
  $( "#journaltabs" ).tabs({active: 0});
});

// item tabs
$(function() {
  $( "#itemtabs" ).tabs({active: 0});
});

// journal hide block
$(function() {
  $('.btn-jslide').click(function() {
    $(this).parent().find('.journal-slide-more').slideToggle(200);
    $(this).toggleClass('active');
    return false;
  });
});

// item slider
$(window).load(function() {
  $('.item__slider').bxSlider({
    mode: 'fade',
    controls: false,
    pagerCustom: '.item__slider-pager',
  });
});


// related slider
$('.items-related-slider').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 1,
});

$(window).load(function() {
  function calcHeightForItems($wrapper, itemCls) {
    $wrapper.waitForMedia(function () {
      var maxHeight = 0;
      $(itemCls, $wrapper).each(function() {
        if ( $(this).height() > maxHeight ) {
          maxHeight = $(this).height();
        }
      });
      $(itemCls, $wrapper).each(function() {
        $(this).css({height : maxHeight});  
      });
    });
  }
  // catalog item height
  calcHeightForItems($('.catalog-items'), '.item');
  // compare item height
  calcHeightForItems($(".comparison-page"), '.item');

  $(document).on('mse2_load', function (response) {
    calcHeightForItems($('.catalog-items'), '.item');
  });
});


// TODO: add waitForMedia
// manufacture item
var compareMaxHeight = 0;
$(".manufacture-item").each(function(){
  if ( $(this).height() > maxHeight ) 
  {
    compareMaxHeight = $(this).height();
  }
});
$(".manufacture-item").height(compareMaxHeight);

// compare duplicate scroll bar
$(document).ready(function() {
   $('.double-scroll').doubleScroll();
});

function initProductnumber(){
    $('div.item-cnt').each(function(){
        var hold = $(this);
        var link = hold.find('input:text');
        var up = hold.find('a.plus');
        var down = hold.find('a.minus');
        var count;
        var maxValue = link.data('max-value') || 100;
        var minValue = link.data('min-value') || 1;
        
        up.off('click').on('click', function(){
            count = link.val()/1;
            if(!count) count = 0;
            count++;
            if (count>maxValue)
                count = maxValue;
            
            if (count > minValue) {
                $(down).removeAttr('disabled').css({pointerEvents: 'auto'});
            }
            
            link.val(count);
            link.trigger('change');
            return false;
        });
        down.off('click').on('click', function(){
            count = link.val()/1;
            if(!count) count = 0;
            count--;
            if(count <= minValue) {
                console.log('fff');
                $(down).attr('disabled', true).css({pointerEvents: 'none'});
            }
            if(count < minValue) { 
                count = minValue;
            }
            link.val(count);
            link.trigger('change');
            return false;
        });
    });
}

// journal item sliders
$('.bxslider-journal-item').bxSlider({
  pagerCustom: '#bx-pager-journal-item',
  controls: false,
  auto: true,
  pause: 7000
});
$('#bx-pager-journal-item').bxSlider({
  minSlides: 5,
  maxSlides: 5,
  slideWidth: 125,
  slideMargin: 10,
  auto: true,
  pause: 7000,
  pager: false,
  nextText: '',
  prevText: '',
});
$('#bx-pager-journal-item2').bxSlider({
  minSlides: 4,
  maxSlides: 4,
  slideWidth: 360,
  slideMargin: 10,
  auto: true,
  pause: 7000,
  pager: false,
  nextText: '',
  prevText: '',
});

// about page
$('.slick-about').slick({
  infinite: true,
  slidesToShow: 5,
  slidesToScroll: 1,
  dots: false,
  autoplay: true,
  autoplaySpeed: 15000,
});