<?php include 'inc/header.php'; ?>
	<!-- page head -->
	<div class="wrapper img-header"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 breadcrumbs">
				<ul>
					<li><a href="#">Главная</a></li><span>&rsaquo;</span>
					<li><a href="#">Журнал AV</a></li><span>&rsaquo;</span>
					<li><a href="#">Конференц-залы</a></li><span>&rsaquo;</span>
					<li><a href="#">Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- tile -->
	<div class="container">
		<header>
			<h1 class="title-journal">Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ</h1>
		</header>
		<div>
			<div class="j-item-date">05 августа 2015, 16:26</div>
			<a href="#" class="j-item-btn-video"></a>
			<span class="j-item-view">92</span>
			<span class="j-item-dollars v1"></span>
			<div class="cliarfix"></div>
		</div>
	</div>

	<!-- content -->
	<div class="container journal-content">
		<div class="row">
			<div class="col-xs-8 column">
				<!-- content -->
				<div class="journal-item-slider-container">
					<ul class="bxslider-journal-item">
						<li><img src="http://images.inout.by/upload/Journal/pvt.jpg" /></li>
						<li><img src="http://images.inout.by/upload/Journal/pvt.jpg" /></li>
					</ul>
				</div>

				<div class="journal-item-slider-pager">
					<div id="bx-pager-journal-item">
						<a data-slide-index="0" href=""><img src="http://images.inout.by/resources/3754/125x83/dsc-9020.jpg" /></a>
						<a data-slide-index="1" href=""><img src="http://images.inout.by/resources/3754/125x83/dsc-9020.jpg" /></a>
					</div>
				</div>
				<div class="row jitem-info">
					<div class="col-xs-7 column">
						<p><strong>Объект:</strong> Парк высоких технологий РБ</p>
						<p><strong>Адрес:</strong> ул. Академика Купревича 1/1, Минск</p>
						<p><strong>Дата реализации:</strong> 2014-09-11 16:34:00</p>
						<p><strong>Срок реализации:</strong> 2 дня</p>
					</div>
					<div class="col-xs-5 column">
						<img src="http://images.inout.by/upload/Journal/pvt_logo.jpg" alt="Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ" style="max-height: 150px; float: right;">
					</div>
				</div>
				<div class="row jitem-text">
					<div class="col-xs-12 column">
							<h2><strong style="background-color: initial;">Оснащение </strong><strong style="background-color: initial;">AV</strong><strong style="background-color: initial;">-оборудованием конференц-зала Парка высоких технологий Республики Беларусь (г. Минск).</strong></h2><p style="text-align: justify;"><strong>Информация о заказчике:</strong> </p><p style="text-align: justify;">Белорусский Парк высоких технологий является виртуальным; его правовой режим действует для предприятий ИКТ-сектора на всей территории республики.  Сегодня в Парке зарегистрированы и работы около 150 компаний, которые занимаются разработкой программных продуктов и предоставлением ИТ-услуг клиентам более чем из 55 стран мира. </p><p style="text-align: justify;">Активное развитие Парка и отрасли в целом стало возможным благодаря серьезной государственной поддержке с выходом в 2005 году Декрета №12 «О Парке высоких технологий» Президента Республики Беларусь Александра Лукашенко. И сегодня отрасль ИКТ является одной из важнейших в национальной экономике.</p><p style="text-align: justify;">Система налоговых льгот способствует конкурентоспособности белорусского ИКТ –сектора на мировых рынках и позволяет поддерживать высокую квалификацию отечественных специалистов.</p><p style="text-align: justify;">Для строительства штаб-квартиры Парка, инфраструктурных объектов, научно-производственной базы в Минске выделено 50 гектаров земли. Согласно проекту, здесь смогут жить, работать и заниматься спортом сотрудники Парка и студенты IT-Академии. <a href="http://www.park.by/?lng=ru">http://www.park.by/?lng=ru</a></p><p style="text-align: justify;"><strong>Краткое описание объекта и задачи проекта.</strong></p><p style="text-align: justify;">Зал-трансформер для проведения конференций, семинаров, симпозиумов, собраний, торжественных мероприятий, концертов.</p><p style="text-align: justify;">Перед специалистами интегратора стояли задачи подобрать профессиональное звуковое оборудование для проведений конференций, собраний и торжественных мероприятий. Главными требованиями заказчика к AV- системе были мобильность, компактность, легко в использовании и надежность.<br> <strong></strong></p><p></p><p><strong>Разработка и осуществление проекта.</strong></p><p><strong>Оказанные услуги:</strong> выбор оборудования согласно проекту, монтаж, пусконаладка и настройка оборудования, обучение персонала пользованию оборудованием.<strong></strong></p><p><strong>Сложности реализации проекта </strong>были связаны с необходимостью создания мобильного комплекса, а также с тем, что в роли заказчика выступали иностранные инвесторы, не говорящие по-русски.<strong></strong></p><p><strong>Реализованный проект, его состав и возможности</strong></p><p>Смонтированный портативный мобильный AV-комлекс позволяет работать как в помещении, так и в полевых условиях. Комплекс рассчитан на аудитория до 100 человек и озвучивание от 1 до 4 спикеров. Радио-микрофоны позволяют перемещаться по залу или сцене на расстоянии до 100 метров. Мобильные акустические и микрофонные стойки позволяют установить колонки и микрофоны.</p><p></p><p><strong>Оборудование для оснащения конференц-зала:</strong></p><p>Радиомикрофоны Biema 2588<br> Радиомикрофон BiemaUHF 58III/SM2&nbsp;<br> Пульт микшерный профессиональный Soundcraft EFX8 <br> Усилитель профессиональный W220III <br> Микрофон проводной Shure PG48 <br> Стойка микрофонная  Athletic MIC-5 <br> Стойка акустическая Athletic BOX-5 <br> Настенный громкоговоритель RCF PL 8X<br> Мобильный комплект American Audio PPA-210<br> Кабель акустический<br> Кабель микрофонный<strong></strong></p><p><strong>Количество&nbsp;</strong><strong></strong><strong>звезд&nbsp;</strong><strong></strong><strong>оборудования</strong><strong> </strong></p><p>Biema -5<br> Shure -8<br> Soundcraft -8<br> Athletic -5<br> RCF-7<br> American Audio-6<br> Cordial-7<br> Neutrick  - 10<br> <strong></strong></p><p style="text-align: justify;"><strong>Перспективы развития проекта:&nbsp;</strong>По желанию заказчика можно увеличить мощность акустических систем, расширить кол-во микрофонов, стоек и т.д.</p><p style="text-align: justify;"><strong>Об исполнителе:</strong> Компания ИНАУТ оказывает полный комплекс услуг по подбору,&nbsp;установке, подключению, настройке и обслуживанию систем профессионального аудиовизуального оборудования в конференц-залах любой степени сложности.  Сотрудничество ИНАУТ с широким спектром поставщиков позволяет решать задачи заказчика как в бюджетном варианте исполнения, так и в дорогих поставках ведущих мировых брендов под заказ. Компетенция профессиональных звукорежиссеров дает гарантии&nbsp;рационального вложения средств по оснащению объектов AV-оборудованием и его надежной эксплуатации. Если заказчик располагает специалистами для установки AV-оборудования, компания ИНАУТ может выполнить шеф-монтаж.</p><p><strong></strong></p>

							<div class="clearfix"></div>
							<h2>Используемые брэнды:</h2>
							<div class="journal-item-slider-pager2">
								<div id="bx-pager-journal-item2">
									<a href=""><img src="img/manufacture.png" alt="manufactures" /></a>
									<a href=""><img src="img/manufacture.png" alt="manufactures" /></a>
									<a href=""><img src="img/manufacture.png" alt="manufactures" /></a>
									<a href=""><img src="img/manufacture.png" alt="manufactures" /></a>
									<a href=""><img src="img/manufacture.png" alt="manufactures" /></a>
									<a href=""><img src="img/manufacture.png" alt="manufactures" /></a>
									<a href=""><img src="img/manufacture.png" alt="manufactures" /></a>
								</div>
							</div>
							<!-- socials -->
							<div class="item-socials">
								<p>
								<span>Поделиться с друзьями:</span>
								</p><div class="social-likes social-likes_visible" data-counters="no" data-url="http://inout.by/catalog/sound-equipment/speakers/177" data-title="Aктивная колонка Alto TS 110A" data-image="http://inout.by/http://images.inout.by/products/177/120x90/ts110a-3.jpg">
								<a class="vk social-likes__widget social-likes__widget_vkontakte" title="Поделиться во Vkontakte"><span class="social-likes__button social-likes__button_vkontakte"><span class="social-likes__icon social-likes__icon_vkontakte"></span></span></a>
								<a class="fb social-likes__widget social-likes__widget_facebook" title="Поделиться в Facebook"><span class="social-likes__button social-likes__button_facebook"><span class="social-likes__icon social-likes__icon_facebook"></span></span></a>
								<a class="tw social-likes__widget social-likes__widget_twitter" data-via="inoutby" data-related="inoutby" title="Поделиться в Twitter"><span class="social-likes__button social-likes__button_twitter"><span class="social-likes__icon social-likes__icon_twitter"></span></span></a>
								<a class="gp social-likes__widget social-likes__widget_plusone" title="Поделиться в Google+"><span class="social-likes__button social-likes__button_plusone"><span class="social-likes__icon social-likes__icon_plusone"></span></span></a>
								</div>
								<p></p>
							</div>
					</div>
				</div>
				<!-- / content -->
				<div class="row">
					<div class="equip-list">
						<div class="col-xs-12"><div class="hr"></div></div>
						<div class="col-xs-6 equip-list_left">
							<header>Список оборудования:</header>
							<ul>
								<li><a href="#">Трансляционный усилитель Fonestar MA-460BZ</a></li>
								<li><a href="#">Комплект усилителя – удлинителя SC&T VE01HA</a></li>
								<li><a href="#">Усилитель – распределитель Shinybow SB-5658</a></li>
							</ul>
						</div>
						<div class="col-xs-6 equip-list_right">
							<header>Приблизительная стоимость</header>
							<div class="cost">56 000 000 Br</div>
						</div>
						<div class="col-xs-12"><div class="hr"></div></div>
					</div>
				</div>

				<div class="row">
					<div class="jitem-prev-next">
						<div class="col-xs-6 column prev">
							<a href="#">Вернуться к выбору проектов</a>
						</div>
						<div class="col-xs-6 column next">
							<a href="#">
								Следующий проект<br>
								<span>(Оснащение AV-оборудованием конференц-зала "VOLAT")</span>
							</a>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="row form-journal form-journal2 gray-container cart-form">
						<form class="form ajax_form" action="" method="post">
							<input type="hidden" name="nospam:blank" value="">
							<div class="col-xs-8 form-header">
								<header>Форма заказа</header>
								<p>Если вас заинтересовали наши услуги напишите нам, мы свяжемся с вами в ближейшее время.</p>
							</div>
							<div class="col-xs-7">
								<label for="af_name">Ваше имя</label>
								<input type="text" name="name" value="" id="af_name">
								<span class="error_name"></span>
							</div>
							<div class="col-xs-7">
								<label for="af_email">Контактная эл. почта</label>
								<input type="text" name="email" value="" id="af_email">
								<span class="error_email"></span>
							</div>
							<div class="col-xs-7">
								<label for="af_message">Кратко опишите заказ</label>
								<textarea type="text" rows="7" id="af_message" name="message"></textarea>
								<span class="error_message"></span>
							</div>
							<div class="col-xs-12">
								<br>
								<button class="btn-default btn-ttu" type="submit">Отправить</button>
							</div>
							<input type="hidden" name="af_action" value="8d6c86e99e2f849878cb4827d40ff5af">
						</form>                        
					</div>
				</div>
			</div>
			<div class="col-xs-4 item-right-side">
				<header class="j-item-right-title">Похожие проекты</header>
				<div class="row">
					<div class="col-xs-12 j-item">
						<a href="#" class="j-item__wrap">
							<div class="j-item__img-container" style="border-color:#0097e9;">
								<img src="img/content/jitem.jpg" alt="item">
							</div>
							<img src="img/content/pvt_logo.jpg" alt="logo">
						</a>
						<a href="#">
							<h2 class="j-item__title">
								Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ
							</h2>
						</a>
						<div class="j-item__desc">
							Зал-трансформер для проведения конференций, семинаров, симпозиумов, собраний.
						</div>
						<div>
							<a href="#" class="j-item-btn-video"></a>
							<span class="j-item-view">92</span>
							<span class="j-item-dollars v1"></span>
							<div class="cliarfix"></div>
						</div>
					</div>
					<div class="col-xs-12 j-item">
						<a href="#" class="j-item__wrap">
							<div class="j-item__img-container" style="border-color:#0097e9;">
								<img src="img/content/jitem.jpg" alt="item">
							</div>
							<img src="img/content/pvt_logo.jpg" alt="logo">
						</a>
						<a href="#">
							<h2 class="j-item__title">
								Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ
							</h2>
						</a>
						<div class="j-item__desc">
							Зал-трансформер для проведения конференций, семинаров, симпозиумов, собраний.
						</div>
						<div>
							<a href="#" class="j-item-btn-video"></a>
							<span class="j-item-view">92</span>
							<span class="j-item-dollars v1"></span>
							<div class="cliarfix"></div>
						</div>
					</div>
					<div class="col-xs-12 j-item">
						<a href="#" class="j-item__wrap">
							<div class="j-item__img-container" style="border-color:#0097e9;">
								<img src="img/content/jitem.jpg" alt="item">
							</div>
							<img src="img/content/pvt_logo.jpg" alt="logo">
						</a>
						<a href="#">
							<h2 class="j-item__title">
								Оснащение AV-оборудованием конференц-зала Парка высоких технологий РБ
							</h2>
						</a>
						<div class="j-item__desc">
							Зал-трансформер для проведения конференций, семинаров, симпозиумов, собраний.
						</div>
						<div>
							<a href="#" class="j-item-btn-video"></a>
							<span class="j-item-view">92</span>
							<span class="j-item-dollars v1"></span>
							<div class="cliarfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



<?php include 'inc/footer.php'; ?>