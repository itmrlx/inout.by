<?php include 'inc/header.php'; ?>
	<!-- page head -->
	<div class="wrapper img-header"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 breadcrumbs">
				<ul>
					<li><a href="#">Главная</a></li><span>&rsaquo;</span>
					<li><a href="#">Обзоры</a></li><span>&rsaquo;</span>
					<li><a href="#">Обзор аккустической системы SONY</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- tile -->
	<div class="container">
		<header>
			<h1 class="title-journal">Обзор аккустической системы SONY</h1>
		</header>
		<div>
			<div class="j-item-date">04 апреля 2016, 09:38</div>
			<a href="#" class="j-item-btn-video"></a>
			<span class="j-item-view">84</span>
			<span class="j-item-dollars v1"></span>
			<div class="cliarfix"></div>
		</div>
	</div>

	<!-- content -->
	<div class="container journal-content">
		<div class="row">
			<div class="col-xs-8 column">
				<!-- content -->
				<div class="journal-item-slider-container">
					<ul class="bxslider-journal-item">
						<li><img src="img/content/obzor-slide.jpg" /></li>
						<li><img src="img/content/obzor-slide.jpg" /></li>
						<li><img src="img/content/obzor-slide.jpg" /></li>
						<li><img src="img/content/obzor-slide.jpg" /></li>
						<li><img src="img/content/obzor-slide.jpg" /></li>
					</ul>
				</div>

				<div class="journal-item-slider-pager">
					<div id="bx-pager-journal-item">
						<a data-slide-index="0" href=""><img src="img/content/obzor-slide.jpg" /></a>
						<a data-slide-index="1" href=""><img src="img/content/obzor-slide.jpg" /></a>
						<a data-slide-index="2" href=""><img src="img/content/obzor-slide.jpg" /></a>
						<a data-slide-index="3" href=""><img src="img/content/obzor-slide.jpg" /></a>
						<a data-slide-index="4" href=""><img src="img/content/obzor-slide.jpg" /></a>
					</div>
				</div>
				<div class="row jitem-info">
					<div class="col-xs-7 column">
						<p><strong>Производитель:</strong> Sony</p>
						<p><strong>Категория:</strong> Акустические системы</p>
						<p><strong>Область применения:</strong> Кафе, бары, рестораны</p>
						<p><strong>Приблизительная стоимость:</strong> 8 000 000 Br</p>
					</div>
					<div class="col-xs-5 column">
						<img src="img/content/sony-logo.png" alt="Sony" style="max-height: 150px; float: right;">
					</div>
				</div>
				<div class="row jitem-text">
					<div class="col-xs-12 column">
							<h2>Описание товара</h2>
							<p>Мы рады представить вам результаты нашей работы для компании ООО «Еврошуз».
							В результате реализации данного проекта была выполнена инсталляция профессионального звукового оборудования в трех магазинах компании Euroshoes в торговых центрах «Замок», «Столица»,  а так же в магазине по адресу ул. Немига, 5.</p>
							<p>Нам приятно сотрудничество с сетью обувных магазинов Euroshoes, которая представляет продукцию наиболее популярных европейских производителей (SALAMANDER, IMAC, Rieker,Wortmann Group:Tamaris, MarcoTozzi, Caprice, S’Oliver, Jana и др.). За многолетний опыт торговли обувью наши клиенты сумели занять прочные позиции на рынке, реализуя изделия высочайшего качества и современного дизайна.</p>
							<br>
							<br>
							<h2>Технические характеристики</h2>
							<p>Наше сотрудничество с компанией Еврошуз построено на понимании клиентом важности музыкального оформления своих мест продаж и нашей возможности удовлетворить эту потребность наилучшим образом. Специалисты «IN/OUT» знают, что, при правильном подборе музыкальных носителей и плейлиста, количество спонтанных продаж можно увеличить на 30-40% и продлить время пребывания потребителя в магазине на 25-35%.</p>
							<img src="img/content/sony-image.png" alt="sony">
							<br>
							<br>
							<h2>Область применения оборудования</h2>
							<p>Музыка оказывает определенное воздействие на человека, его настроение, самочувствие. Даже если покупатель не обращает внимание на стандартные виды рекламы – телереклама, банеры и прочее, то от воздействия музыки он абстрагироваться не может. </p>
							<p>Весьма интересны результаты исследований, которые доказывают, что медленная инструментальная музыка может увеличить среднее время нахождения клиента в магазине 
							на 17%, а средний чек — на 38%. С помощью музыки клиентов можно не только задержать, 
							но и ускорить — для этого рекомендуется использовать музыку с быстрым темпом, которая способствует учащению сердцебиения.</p>
							<p>Музыкальные носители – это важная составляющая успеха, наряду с правильным подбором плейлиста. Но даже если музыкальный контент будет правильно составлен, проект музыкального оформления магазина может не принести нужного эффекта, если Вы сэкономили на хорошей акустической системе. Компания «IN/OUT» всегда стремимся найти индивидуальный подход к выполнению заказа и особое внимание уделяет достижению поставленных клиентом целей. Наши специалисты подберут для Вас качественную акустическую систему с учетом особенностей Вашего помещения и Вы останетесь довольны той новой доброжелательной атмосферой, способствующей совершению покупок.</p>
							<br>
							<br>
							<div class="clearfix"></div>
							<!-- socials -->
							<div class="item-socials">
								<p>
								<span>Поделиться с друзьями:</span>
								</p><div class="social-likes social-likes_visible" data-counters="no" data-url="http://inout.by/catalog/sound-equipment/speakers/177" data-title="Aктивная колонка Alto TS 110A" data-image="http://inout.by/http://images.inout.by/products/177/120x90/ts110a-3.jpg">
								<a class="vk social-likes__widget social-likes__widget_vkontakte" title="Поделиться во Vkontakte"><span class="social-likes__button social-likes__button_vkontakte"><span class="social-likes__icon social-likes__icon_vkontakte"></span></span></a>
								<a class="fb social-likes__widget social-likes__widget_facebook" title="Поделиться в Facebook"><span class="social-likes__button social-likes__button_facebook"><span class="social-likes__icon social-likes__icon_facebook"></span></span></a>
								<a class="tw social-likes__widget social-likes__widget_twitter" data-via="inoutby" data-related="inoutby" title="Поделиться в Twitter"><span class="social-likes__button social-likes__button_twitter"><span class="social-likes__icon social-likes__icon_twitter"></span></span></a>
								<a class="gp social-likes__widget social-likes__widget_plusone" title="Поделиться в Google+"><span class="social-likes__button social-likes__button_plusone"><span class="social-likes__icon social-likes__icon_plusone"></span></span></a>
								</div>
								<p></p>
							</div>
					</div>
				</div>
				<!-- / content -->
				<div class="row">
					<div class="equip-list">
						<div class="col-xs-12"><div class="hr"></div></div>
						<div class="col-xs-6 equip-list_left">
							<header>Похожие товары в наличии:</header>
							<ul>
								<li><a href="#">Quadrant (v2)	</a></li>
								<li><a href="#">Linpack single-thread (MFLOPS)</a></li>
								<li><a href="#">Linpack multi-thread (MFLOPS)</a></li>
								<li><a href="#">NenaMark2 (fps)</a></li>
								<li><a href="#">NenaMark1 (fps)</a></li>
							</ul>
						</div>
						<div class="col-xs-6 equip-list_right">
							<header>Приблизительная стоимость</header>
							<div class="cost">8 000 000 Br</div>
						</div>
						<div class="col-xs-12"><div class="hr"></div></div>
					</div>
				</div>

				<div class="row">
					<div class="jitem-prev-next">
						<div class="col-xs-6 column prev">
							<a href="#">Предыдущий обзор</a>
						</div>
						<div class="col-xs-6 column next">
							<a href="#">
								Следующий обзор
							</a>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="row form-journal form-journal2 gray-container cart-form">
						<form class="form ajax_form" action="" method="post">
							<input type="hidden" name="nospam:blank" value="">
							<div class="col-xs-8 form-header">
								<header>Подпишитесь на новости</header>
								<p>Получайте свежу информацию о новинках первыми.</p>
							</div>
							<div class="col-xs-7">
								<label for="af_email">Ваша электронная почта</label>
								<input type="text" name="email" value="" id="af_email">
								<span class="error_email"></span>
							</div>
							<div class="col-xs-12">
								<br>
								<button class="btn-default btn-ttu" type="submit">Подписаться</button>
							</div>
						</form>                        
					</div>
				</div>
			</div>
			<div class="col-xs-4 item-right-side">
				<header class="j-item-right-title">Мнения экспертов</header>
				<div class="row">
					<div class="col-xs-12 expert">
						<img src="img/content/expert1.png" alt="expert">
						<div class="name">Шибаев Андрей</div>
						<div class="status">Исполнительный директор KVINTO, Digital Signage-эксперт</div>
						<div class="desc">
							<p class="cit">— Лучшее блюдо в Digital Signage - то, что сервировано с умом</p>
							“Точное понимание и верное формулирование конечных целей использования Digital Signage — важны для воздействия  на целевую аудиторию.”
						</div>
					</div>
					<div class="col-xs-12 expert">
						<img src="img/content/expert2.png" alt="expert">
						<div class="name">Марченко Мила</div>
						<div class="status">Главный редактор Digital Signage UA Review</div>
						<div class="desc">
							<p class="cit">— Три шаблона Digital Signage для трёх типов зрителей</p>
							“Как быть, если ваша аудитория – это множество разных людей с разными вкусами и взглядами? Рассмотрим три основных типа зрителей…”
						</div>
					</div>
					<div class="col-xs-12 expert-banner">
						<a href="#">
							<img src="img/content/expert-b.png" alt="banner">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>



<?php include 'inc/footer.php'; ?>