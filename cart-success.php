<?php include 'inc/header.php'; ?>
	<!-- page head -->
	<div class="wrapper img-header"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-9 breadcrumbs">
				<ul>
					<li><a href="#">Главная</a></li><span>&rsaquo;</span>
					<li><a href="#">Корзина</a></li>
				</ul>
			</div>
			<div class="col-xs-3">
				<a class="back-to-cat-btn"><span>Вернуться в каталог</span></a>
			</div>
		</div>
	</div>

	<!-- cart success -->
	<section class="container cart-success">
		<!-- CART -->
		<div class="row">
			<div class="col-xs-12 cart-success">
				<!-- header -->
				<header>
					<h1 class="title">Получилось!</h1>
					<p class="p-title-cart">Ваш заказ успешно оформлен!</p>
				</header>
				<div class="row">
					<div class="col-xs-8 cart-success-block">
						<header>Номер заказа: №1122334455</header>
						<p>Бесплатная доставка</p>
						<p>Итого: 90 000 000 Br</p>
					</div>
				</div>
				<p class="senk">Спасибо что выбрали наш интернет-магизин!<br>
				Мы будем рады видеть вас снова!</p>
			</div>
		</div>
		<!-- / CART -->
	</section>


<?php include 'inc/footer.php'; ?>