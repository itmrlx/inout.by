<?php include 'inc/header.php'; ?>
	<!-- page head -->
	<div class="wrapper img-header"></div>
	<div class="container wr-breadcrumbs">
		<div class="row">
			<div class="col-xs-9 breadcrumbs">
				<ul>
					<li><a href="#">Главная</a></li><span>&rsaquo;</span>
					<li><a href="#">Корзина</a></li>
				</ul>
			</div>
			<div class="col-xs-3">
				<div class="comparison comparison-26" data-id="177" data-list="26">
					<a href="#" class="comparison-add btn-add-compare" data-text="Обновляю список...">Добавить в сравнение</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container print-kp">
		Коммерческое предложение
	</div>

	<div class="container item-old">
		<div class="row">
			<div class="col-xs-7 item-left-side">
				<!-- header -->
				<header class="item__header-old">
					<h1 class="item__title-old">Aктивная колонка Alto TS 110A</h1>
					<div class="feedback">
						<div class="rating" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
							<ul class="star-rating-inout" style="width:100px">
								<li class="current-rating" itemprop="ratingValue" style="width:62%;">3.11</li>
								<li class="star"><span>1</span></li>
								<li class="star"><span>2</span></li>
								<li class="star"><span>3</span></li>
								<li class="star"><span>4</span></li>
								<li class="star"><span>5</span></li>
							</ul>
							<span itemprop="ratingCount" class="votes">44</span>
						</div>			
						<div class="review-cnt">
							<a href="http://inout.by/catalog/sound-equipment/speakers/177#review"><span>нет отзывов</span></a>
						</div>
					</div>
				</header>
				<div class="left-desc">
					<p>300 Вт / 10" + 1" / 120 дБ</p>
					<span>Артикул: SP-ALT-001</span>
				</div>
				<div class="item__slider-wrapper">
					<div class="item__slider-container">
						<div class="item__slider">
							<img src="img/content/item-slide.jpg" />
							<img src="img/content/item-slide2.jpg" />
							<img src="img/content/item-slide.jpg" />
							<img src="img/content/item-slide2.jpg" />
							<img src="img/content/item-slide2.jpg" />
							<img src="img/content/item-slide2.jpg" />
							<img src="img/content/item-slide2.jpg" />
						</div>
						<div class="item__slider-pager">
							<a data-slide-index="0" href=""><img src="img/content/item-slide.jpg" /></a>
							<a data-slide-index="1" href=""><img src="img/content/item-slide2.jpg" /></a>
							<a data-slide-index="2" href=""><img src="img/content/item-slide.jpg" /></a>
							<a data-slide-index="3" href=""><img src="img/content/item-slide2.jpg" /></a>
							<a data-slide-index="4" href=""><img src="img/content/item-slide2.jpg" /></a>
							<a data-slide-index="5" href=""><img src="img/content/item-slide2.jpg" /></a>
							<a data-slide-index="6" href=""><img src="img/content/item-slide2.jpg" /></a>
						</div>
						<img src="img/content/alto_logo.png" alt="Alto" class="product-logo">
					</div>
				</div>
				<div class="item-characters print-item-char">
					<h3>Технические характеристики:</h3>
					<table>
						<tbody>
							<tr><td>Тип</td><td>Топ</td></tr>
							<tr><td>Мощность</td><td>300 Вт</td></tr>
							<tr><td>Динамик</td><td>10″</td></tr>
							<tr><td>100 / 70 V</td><td>Нет</td></tr>
							<tr><td>Усилитель</td><td>Да</td></tr>
						</tbody>
					</table>
				</div>
				<!-- tabs -->
				<div id="itemtabs" class="col-xs-12 item-content">
					<ul>
						<li><a href="#tabs-1">Описание</a></li>
						<li><a href="#tabs-2">Характеристики</a></li>
						<li><a href="#tabs-3">Отзывы</a></li>
					</ul>
					<div id="tabs-1">
						<h2><strong>Alto TS 110A</strong></h2>
						<p>Ставшая достаточно известной компания ALTO с невероятной гордостью представляет Вашему вниманию активную акустическую систему ALTO TRUESONIC TS110A. В своей спецификации представленная акустическая система&nbsp;TRUESONIC TS110A&nbsp;представляет активную, двух-полосную акустическую систему с bi-amp усилителем. Сферы для применения представленной акустической системы TS110A достаточно широки, начиная от самых небольших презентационных проектов, заканчивая качественным озвучиванием и проведением различных типов музыкальных мероприятий. Сам производитель этой акустической системы АЛТО ТРУСОНИК ТС110А не так давно появился на музыкальном рынке, но не смотря на это, компания сумела быстро завоевать популярность в среде музыкантов и звукорежиссёров благодаря высокому качеству, к которому производитель предъявляют самые высокие требования мирового стандарта качества.&nbsp;<br><br>Все музыкальное оборудование бренда ALTO, к которому относится представленная акустическая система TRUESONIC TS110A, изготавливается по принципу «профессионалы для профессионалов», что является основным девизом этой компании, который и стал зримым отражением в конечном качестве своей продукции. Акустическая система имеет номинальную мощность в 300 Ватт (RMS), что позволяет достаточно качественно и хорошо озвучивать небольшие и среднего размера помещения, а так же небольшие концертные площадки. В пиковой мощности акустическая система может выдавать до 600 Ватт мощности, без каких либо слышимых акустических искажений. Так как основной и главной идеей, которая преследует компания Alto, является производство аудиопродукции высокого качества, отвечающей всем самым современным требованиям качества и надежности, причем по приемлемой цене, представленная акустическая система ALTO TS110A стала логическим разрешением этой идеи.</p>
						<h3>Основные функциональные особенности ALTO TS110A.</h3>
						<p>Данная акустическая система представлена в серии bi-amp, имеющей активный усилитель и двухполосные звуковые излучатели. Характеристикой мощности являются номинальные показатели, выраженные в 300 Ватт(ах) RMS (235 Ватт НЧ + 65 Ватт ВЧ) и пиковой мощности системы в 600 Ватт (470 Ватт НЧ + 130 Ватт ВЧ). В номинальных показателях характеристики частотного диапазона АС при -10 dB составляют показатели от 53 Герц до 20 кГц. Частотные характеристики при ±3 dB составляют от 70 Герц до 19 кГц. Звуковые излучатели акустических систем представлены низкочастотным (НЧ) вуфером, размером в 10 дюймов с 1,5-дюймовой катушкой. Высокочастотный излучатель оснащен качественным неодимовым драйвером с одно-дюймовой катушкой. Усилитель АС активного типа, класса «D», есть встроенный активный кроссовер с активной частотой в 2,5 кГц. Направленность акустической системы составляет 8 показатели в 80 \ 100 х 60 градусов.<br><br>Вся настройка и управление акустикой осуществляется при помощи ручек регулировки уровня входной чувствительности (Vol. 1 \ 2), работающей для линейного входа (Line) или же входа для микрофонов (Mic). Настройка каналов Volume 1 \ 2 имеют диапазон от + 50 dB, с максимальными характеристиками входного сигнала в +23 dBu. Дополнительные переключатели Ground, Contour и переключатель Power имеют стандартные режимы работы - оn\off. Система оснащена светодиодной индикацией функций Signal limit и Power. Дополнительно есть встроенная система охлаждения, имеющая независимый контур электроники с кулером охлаждения. Коммутационные разъемы системы имеют (Input 1\2) комбинированные разъёмы XLR и стандартный Jack (1\4). Разьем Mix Out имеет линейные характеристики, сквозной, симметричный канал с выходом на XLR разъёме.<br><br></p>
						<h3>Технические характеристики акустической системы Alto TS110A</h3>
						<p><span>Тип устройства : система акустическая.</span><br><span>Спецификация\особенности : bi-amp активная система.</span><br><span>Звуковые излучатели : двухполосные излучатели.</span><br><span>Мощность АС :&nbsp;</span><br><span>- Мощность номинальная : 300 Ватт RMS (235 Ватт НЧ + 65 Ватт ВЧ);</span><br><span>- Мощность пиковая : 600 Ватт (470 Ватт НЧ + 130 Ватт ВЧ).&nbsp;</span><br><span>Характеристики частот (-10 dB) : 53 Гц - 20 кГц.</span><br><span>Частотная характеристика (±3 dB) : 70 Гц - 19 кГц.</span><br><span>Давление звуковое (@ 1 м.) : 123 дБ - пиковое, 120 дБ - продолжительное.</span><br><span>Особенности\характеристики звуковых излучателей :&nbsp;</span><br><span>- Низкочастотный излучатель : размер – 10 дюймов, вуфер - с 1,5-дюймовой катушкой;</span><br><span>- Высокочастотный излучатель : драйвер неодимовый с 1-дюймовой катушкой.</span><br><span>Усилитель : класс D.</span><br><span>Кроссовер системы : активный кроссовер.</span><br><span>Частота кроссовера : 2,5 кГц.</span><br><span>Направленность системы : 80\100 х 60 гр.&nbsp;</span><br><span>Форма\тип корпуса : трапециевидная форма.</span><br><span>Материал корпуса : пластик.</span><br><span>Решетка защитная : сталь.&nbsp;</span><br><span>Особенности эксплуатации АС :&nbsp;</span><br><span>- Стандартная фронтальная система;</span><br><span>- Возможность установки в качестве сценического, напольного монитора.&nbsp;</span><br><span>Настройки \ управление АС :&nbsp;</span><br><span>- Настройки входной чувствительности (Vol. 1\2) : для линии (Line) и микрофона (Mic);&nbsp;</span><br><span>- Настройка канала Vol. 1 \ 2 : + 50 dB, максим. входн. уровень +23 dBu;</span><br><span>- Переключатель Ground : режимы с on\off;</span><br><span>- Переключатель Contour : режимы с on\off;</span><br><span>- Переключатель Power : режимы с on\off.</span><br><span>Индикация : светодиодная, функции Signal limit и Power.</span><br><span>Система охлаждения : принудительная, встроенная.&nbsp;</span><br><span>Коммутационные разъемы :&nbsp;</span><br><span>- Input 1 \ 2 : разъёмы комбинированные - XLR и Jack (1\4);</span><br><span>- Mix Out : разьем сквозной, линейный, симметричный выход на XLR разъёме.</span><br><span>Установка : гнезда (стаканы) для стоек (36 мм.).</span><br><span>Транспортировка : транспортировочные ручки, две.</span><br><span>Точки для подвеса : пять точек подвеса с резьбой М10.</span><br><span>Цвет системы : черный цвет.</span><br><span>Размеры АС : (Ш) 297 мм. x (Д) 329 мм. x (В) 513 миллиметров.</span><br><span>Вес АС : 12,1 кг.</span></p>
					</div>
					<div id="tabs-2" class="item-characters">
						<table>
							<tbody>
								<tr><td>Тип</td><td>Топ</td></tr>
								<tr><td>Мощность</td><td>300 Вт</td></tr>
								<tr><td>Динамик</td><td>10″</td></tr>
								<tr><td>100 / 70 V</td><td>Нет</td></tr>
								<tr><td>Усилитель</td><td>Да</td></tr>
							</tbody>
						</table>
					</div>
					<div id="tabs-3">
						<form id="comment-form" action="" method="post" class="row review-form gray-container cart-form">
							<input type="hidden" name="thread" value="resource-177">
							<input type="hidden" name="parent" value="0">
							<input type="hidden" name="id" value="0">

							<div class="col-xs-12 head-review"><h2>Оставить отзыв</h2></div>
							<div class="col-xs-5">
								<label for="comment-name">Ваше имя</label>
								<input type="text" name="name" value="" id="comment-name">
								<span class="error"></span>
							</div>
							<div class="col-xs-7">
								<label for="comment-email">Контактная эл. почта</label>
								<input type="text" name="email" value="" id="comment-email">
								<span class="error"></span>
							</div>
							<div class="col-xs-12">
								<label for="comment-editor">Отзыв</label>
								<textarea name="text" id="" cols="30" rows="3"></textarea>
							</div>

							<div class="col-xs-12">
								<label for="comment-captcha" id="comment-captcha">Введите сумму 5 + 7</label>
								<input type="text" name="captcha" value="" id="comment-captcha">
								<span class="error"></span>
							</div>
							<div class="col-xs-12">
								<br>
								<br>
								<button type="submit" class="btn-default btn-ttu">Опубликовать</button>
							</div>
							<span class="time"></span>
						</form>
					</div>
				</div>
			</div>
			<!-- right side -->
			<div class="col-xs-5 item-right-side">
				<a href="javascript:window.print(); void 0;" class="btn-print alignright">Печать страницы</a>
				<div class="clearfix"></div>
				<div class="print-item-head">
					<h1>Aктивная колонка Alto TS 110A</h1>
					<p><span>Артикул: SP-ALT-001</span></p>
					<p>Стоимость:</p>
				</div>
				<div class="item__description-old" itemprop="description">
					Alto TS 110A<br>
					Ставшая достаточно известной компания ALTO с невероятной гордостью представляет Вашему вниманию активную акустическую систему ALTO TRUESONIC TS110A. В своей спецификации представленная акустическая система&nbsp;TRUESONIC TS110A&nbsp;представляет активную, двух-полосную акустическую систему с…<br>
				</div>
				<div class="item__price">
					<span class="cost-bel">
						<span itemprop="price" content="6571000">6 571 000</span> 
						<span itemprop="priceCurrency" content="BYR">Br</span>
					</span>
				</div>
				<button type="submit" class="btn-default big btn-ttu">Заказать</button>
				<div class="buy-list">
					<link itemprop="availability" href="http://schema.org/InStock" content="In Stock">
					<ul>
						<li class="ico-stock-blue">Есть в наличии</li>
						<li class="ico-delivery">Доставка в течении 1-2 дней</li>
						<li class="ico-pay">Оплата наличными или безналичными</li>
						<li class="ico-garanty">Гарантия 1 год</li>
						<li class="ico-replace">Обмен в течении 14 дней</li>
					</ul>
				</div>
				<div class="item-socials">
					<p>
					<span>Поделиться с друзьями:</span>
					</p><div class="social-likes social-likes_visible" data-counters="no" data-url="http://inout.by/catalog/sound-equipment/speakers/177" data-title="Aктивная колонка Alto TS 110A" data-image="http://inout.by/http://images.inout.by/products/177/120x90/ts110a-3.jpg">
					<a class="vk social-likes__widget social-likes__widget_vkontakte" title="Поделиться во Vkontakte"><span class="social-likes__button social-likes__button_vkontakte"><span class="social-likes__icon social-likes__icon_vkontakte"></span></span></a>
					<a class="fb social-likes__widget social-likes__widget_facebook" title="Поделиться в Facebook"><span class="social-likes__button social-likes__button_facebook"><span class="social-likes__icon social-likes__icon_facebook"></span></span></a>
					<a class="tw social-likes__widget social-likes__widget_twitter" data-via="inoutby" data-related="inoutby" title="Поделиться в Twitter"><span class="social-likes__button social-likes__button_twitter"><span class="social-likes__icon social-likes__icon_twitter"></span></span></a>
					<a class="gp social-likes__widget social-likes__widget_plusone" title="Поделиться в Google+"><span class="social-likes__button social-likes__button_plusone"><span class="social-likes__icon social-likes__icon_plusone"></span></span></a>
					</div>
					<p></p>
				</div>
				<div class="clearfix"></div>
				<div class="print-item-desc">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure sequi dolores adipisci soluta ut eaque dolore provident, natus omnis! Doloribus!</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus accusamus quisquam impedit nobis odit mollitia error ex repellat sint dolore, ratione magni, quasi officia.</p>
				</div>
				<div class="small-12 add-desc">
					<p><strong>Срок службы:</strong> не менее 5 лет (при соблюдении правил хранения, ухода и эксплуатации)</p>
					<p><strong>Импортер:</strong> ООО "Глобал Саунд", 220140, г.Минск, ул.Тимошенко, д.8 пом.19Н</p>
					<p><strong>Гарантийное облуживание:</strong> ИП Якунин Р.М., УНП 191482637</p>
					<p><strong>Производитель:</strong> Alto <a href="http://inout.by/buyer/manufacturers">(адрес)</a></p>
					<p><strong>Страна производства:</strong> Италия</p>
					<p><a href="http://inout.by/buyer/care">Правила ухода и эксплуатации</a></p>
				</div>
			</div>
		</div>
	</div>
	<div class="wrapper print-item-services">
		<div class="container">
			<header>Услуги нашей компании</header>
			<div class="row">
				<div class="sp2">
					<a href="#" class="main-service__title main-service__t1">
						<img src="img/s1small.png" alt="">
						<span>Проектирование</span>
					</a>
				</div>
				<div class="sp2">
					<a href="#" class="main-service__title main-service__t3">
						<img src="img/s3small.png" alt="">
						<span>Монтаж</span>
					</a>
				</div>
				<div class="sp2">
					<a href="#" class="main-service__title main-service__t5">
						<img src="img/s5small.png" alt="">
						<span>Настройка</span>
					</a>
				</div>
				<div class="sp2 sp2-offset-right-2">
					<a href="#" class="main-service__title main-service__t7">
						<img src="img/s7small.png" alt="">
						<span>Техническая поддержка</span>
					</a>
				</div>
				<div class="sp2">
					<a href="#" class="main-service__title main-service__t2">
						<img src="img/s2small.png" alt="">
						<span>Поставка</span>
					</a>
				</div>
				<div class="sp2">
					<a href="#" class="main-service__title main-service__t4">
						<img src="img/s4small.png" alt="">
						<span>Подключение</span>
					</a>
				</div>
				<div class="sp2">
					<a href="#" class="main-service__title main-service__t6">
						<img src="img/s6small.png" alt="">
						<span>Обучение персонала</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 items-related">
				<header>Похожие товары</header>
				<div class="items-related-slider">
					<!-- items -->
					<div class="slide">
						<div class="catalog-item">
							<a href="#" class="catalog-item_img">
								<img src="img/content/item.jpg" alt="item">
							</a>
							<button type="submit" class="btn-default small" name="ms2_action" value="cart/add" style="width: 100%;">В корзину</button>
							<a href="#" class="catalog-item__desc-container">
								<span class="catalog-item__desc">Активная колонка</span>
								<span class="catalog-item__mark">RCF ART 310-A</span>
							</a>
							<a href="#" class="catalog-item__add-compare"> <!-- link add to compare -->
								<span>Сравнить</span>
							</a>
							<a href="#" class="catalog-item__small-last">
								<span class="catalog-item__cost">13 032 000 Br</span>
								<span class="catalog-item__tech">400 Вт / 10" + 1" / 127 дБ</span>
							</a>
						</div>
					</div>
					<div class="slide">
						<div class="catalog-item">
							<a href="#" class="catalog-item_img">
								<img src="img/content/item.jpg" alt="item">
							</a>
							<button type="submit" class="btn-default small" name="ms2_action" value="cart/add" style="width: 100%;">В корзину</button>
							<a href="#" class="catalog-item__desc-container">
								<span class="catalog-item__desc">Активная колонка</span>
								<span class="catalog-item__mark">RCF ART 310-A</span>
							</a>
							<a href="#" class="catalog-item__add-compare"> <!-- link add to compare -->
								<span>Сравнить</span>
							</a>
							<a href="#" class="catalog-item__small-last">
								<span class="catalog-item__cost">13 032 000 Br</span>
								<span class="catalog-item__tech">400 Вт / 10" + 1" / 127 дБ</span>
							</a>
						</div>
					</div>
					<div class="slide">
						<div class="catalog-item">
							<a href="#" class="catalog-item_img">
								<img src="img/content/item.jpg" alt="item">
							</a>
							<button type="submit" class="btn-default small" name="ms2_action" value="cart/add" style="width: 100%;">В корзину</button>
							<a href="#" class="catalog-item__desc-container">
								<span class="catalog-item__desc">Активная колонка</span>
								<span class="catalog-item__mark">RCF ART 310-A</span>
							</a>
							<a href="#" class="catalog-item__add-compare"> <!-- link add to compare -->
								<span>Сравнить</span>
							</a>
							<a href="#" class="catalog-item__small-last">
								<span class="catalog-item__cost">13 032 000 Br</span>
								<span class="catalog-item__tech">400 Вт / 10" + 1" / 127 дБ</span>
							</a>
						</div>
					</div>
					<div class="slide">
						<div class="catalog-item">
							<a href="#" class="catalog-item_img">
								<img src="img/content/item.jpg" alt="item">
							</a>
							<button type="submit" class="btn-default small" name="ms2_action" value="cart/add" style="width: 100%;">В корзину</button>
							<a href="#" class="catalog-item__desc-container">
								<span class="catalog-item__desc">Активная колонка</span>
								<span class="catalog-item__mark">RCF ART 310-A</span>
							</a>
							<a href="#" class="catalog-item__add-compare"> <!-- link add to compare -->
								<span>Сравнить</span>
							</a>
							<a href="#" class="catalog-item__small-last">
								<span class="catalog-item__cost">13 032 000 Br</span>
								<span class="catalog-item__tech">400 Вт / 10" + 1" / 127 дБ</span>
							</a>
						</div>
					</div>
					<div class="slide">
						<div class="catalog-item">
							<a href="#" class="catalog-item_img">
								<img src="img/content/item.jpg" alt="item">
							</a>
							<button type="submit" class="btn-default small" name="ms2_action" value="cart/add" style="width: 100%;">В корзину</button>
							<a href="#" class="catalog-item__desc-container">
								<span class="catalog-item__desc">Активная колонка</span>
								<span class="catalog-item__mark">RCF ART 310-A</span>
							</a>
							<a href="#" class="catalog-item__add-compare"> <!-- link add to compare -->
								<span>Сравнить</span>
							</a>
							<a href="#" class="catalog-item__small-last">
								<span class="catalog-item__cost">13 032 000 Br</span>
								<span class="catalog-item__tech">400 Вт / 10" + 1" / 127 дБ</span>
							</a>
						</div>
					</div>
					<div class="slide">
						<div class="catalog-item">
							<a href="#" class="catalog-item_img">
								<img src="img/content/item.jpg" alt="item">
							</a>
							<button type="submit" class="btn-default small" name="ms2_action" value="cart/add" style="width: 100%;">В корзину</button>
							<a href="#" class="catalog-item__desc-container">
								<span class="catalog-item__desc">Активная колонка</span>
								<span class="catalog-item__mark">RCF ART 310-A</span>
							</a>
							<a href="#" class="catalog-item__add-compare"> <!-- link add to compare -->
								<span>Сравнить</span>
							</a>
							<a href="#" class="catalog-item__small-last">
								<span class="catalog-item__cost">13 032 000 Br</span>
								<span class="catalog-item__tech">400 Вт / 10" + 1" / 127 дБ</span>
							</a>
						</div>
					</div>
					<!-- / items -->
				</div>
			</div>
		</div>
	</div>

<?php include 'inc/footer.php'; ?>