<?php include 'inc/header.php'; ?>
	<!-- page head -->
	<div class="wrapper img-header"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 breadcrumbs">
				<ul>
					<li><a href="#">Главная</a></li><span>&rsaquo;</span>
					<li><a href="#">Корзина</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- tile -->
	<div class="container">
		<header>
			<h1 class="page-title">Производители</h1>
		</header>
	</div>

	<!-- manufactures -->
	<div class="container manufactures">
		<div class="row">
			<div class="col-xs-12">
				<div class="manufactures-menu">
					<div class="row">
						<div class="col-xs-3">
							<a href="#" class="btn btn-primary">Все производители</a>
						</div>
						<div class="col-xs-9 alphabet">
							<a href="#">0-9</a>
							<a href="#">A</a>
							<a href="#">B</a>
							<a href="#">C</a>
							<a href="#">D</a>
							<a href="#">E</a>
							<a href="#">F</a>
							<a href="#">G</a>
							<a href="#">H</a>
							<a href="#">I</a>
							<a href="#">J</a>
							<a href="#">K</a>
							<a href="#">L</a>
							<a href="#">M</a>
							<a href="#">N</a>
							<a href="#">O</a>
							<a href="#">P</a>
							<a href="#">Q</a>
							<a href="#">R</a>
							<a href="#">S</a>
							<a href="#">T</a>
							<a href="#">U</a>
							<a href="#">V</a>
							<a href="#">W</a>
							<a href="#">X</a>
							<a href="#">Y</a>
							<a href="#">Z</a>
							<a href="#">А–Я</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<a href="#" class="col-xs-4 manufacture-item">
				<img src="img/manufacture.png" alt="manufacture">
				<h2 class="title">Adam Hall, Германия</h2>
			</a>
			<a href="#" class="col-xs-4 manufacture-item">
				<img src="img/manufacture.png" alt="manufacture">
				<h2 class="title">Adam Hall, Германия</h2>
			</a>
			<a href="#" class="col-xs-4 manufacture-item">
				<img src="img/manufacture.png" alt="manufacture">
				<h2 class="title">Adam Hall, Германия</h2>
			</a>
			<a href="#" class="col-xs-4 manufacture-item">
				<img src="img/manufacture.png" alt="manufacture">
				<h2 class="title">Adam Hall, Германия</h2>
			</a>
			<a href="#" class="col-xs-4 manufacture-item">
				<img src="img/manufacture.png" alt="manufacture">
				<h2 class="title">Adam Hall, Германия</h2>
			</a>
			<a href="#" class="col-xs-4 manufacture-item">
				<img src="img/manufacture.png" alt="manufacture">
				<h2 class="title">Adam Hall, Германия</h2>
			</a>
			<a href="#" class="col-xs-4 manufacture-item">
				<img src="img/manufacture.png" alt="manufacture">
				<h2 class="title">Adam Hall, Германия</h2>
			</a>
			<a href="#" class="col-xs-4 manufacture-item">
				<img src="img/manufacture.png" alt="manufacture">
				<h2 class="title">Adam Hall, Германия</h2>
			</a>
			<a href="#" class="col-xs-4 manufacture-item">
				<img src="img/manufacture.png" alt="manufacture">
				<h2 class="title">Adam Hall, Германия</h2>
			</a>
			<div class="clearfix"></div>
			<div class="centered">
				<a href="#" class="btn btn-default big">Показать еще</a>
			</div>
		</div>
	</div>


<?php include 'inc/footer.php'; ?>